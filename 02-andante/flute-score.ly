\version "2.12.3"
\include "global.ly"
\include "flute-music.ly"

\header {
  instrument = "Flute"
}

\score {
  \new Staff {\global \fluteMusic}
}