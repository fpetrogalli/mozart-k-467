\version "2.12.3"
\include "global.ly"
\include "bassoon-music.ly"

\header {
  instrument = "Basson"
}


\score {
  \new Staff {
%%%    \partcombine {\global \bassoonFirst} {\global \bassoonSecond}
    \partcombine {\relative c' {\clef bass \global  \bassoonFirst}} {\relative c' {\clef bass \global \bassoonSecond}}
  }
}