\version "2.12.3"

\include "global.ly"
\include "violinII-music.ly"
\header {
  instrument = "Violin II"
}

\score {
  \new Staff {
    \global \violinII
  }
}