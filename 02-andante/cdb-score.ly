\version "2.12.3"

\include "global.ly"
\include "cdb-music.ly"

\header {
  instrument = "Cello - Double bass"
}

\score {
  \new Staff {
    \global \cdb 
  }
}