\version "2.12.3"

violinI=\relative c'{
  \clef treble
  % page 1
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r
  g'2~ g8 fis-. g-. e-. |
  f?2~ f8 e-. f-. d-. |
  c4 r r2 |
				%page 2
}