\version "2.12.3"



global = {
  \key f \major
  \time 4/4
  \tempo "Andante" 
  \compressFullBarRests
}


\paper {
  % between-system-padding = #0.1
  % between-system-space = #0.1
  ragged-last-bottom = ##f
  ragged-bottom = ##f
}

\header { 
    title = \markup  { "Piano Concerto K. 467" }
    subtitle = "Andante"
    composer = "W. A. Mozart"
    tagline = "Edited by Francesco Petrogalli"
}


