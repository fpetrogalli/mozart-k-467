\version "2.12.3"

\include "global.ly"
\include "violinI-music.ly"
\header {
  instrument = "Violin I"
}

\score {
  \new Staff {
    \global \violinI
  }
}