\version "2.12.3"


\include "global.ly"

\include "viola-music.ly"
\header {
  instrument = "Viola"
}

\score {
  \new Staff {
    \global \viola
  }
}