\version "2.12.3"

\parallelMusic #'(bassoonFirst bassoonSecond) {
  %%1
  R1*6 |
  R1*6 |
  %%7
  c4\p c8. c16 d4 d8. d16 |
  g4\p g8. g16 b4 b8. b16 |
  %8
  e4 b8-. c-. g4 r |
  c4 b,8-. c-. g4 r |

  R1*2 |
  R1*2 |
  
}

