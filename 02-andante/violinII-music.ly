\version "2.12.3"

violinII=\relative c'{
  \clef treble
  % page 1
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r
  r8 g g g g4 r |
  r8 <d b'>8 <d b'>8 <d b'>8 <d b'>4 r |
  R1*2 | 
  				%page 2 
}