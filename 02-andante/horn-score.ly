\version "2.12.3"
\include "global.ly"
\include "horn-music.ly"

\header {
  instrument = "Horn"
}

\score {
  \new Staff {
%%%    \partcombine {\global \hornFirst} {\global \hornSecond}
    \partcombine {\relative c'' {\clef treble \global  \hornFirst}} {\relative c' {\clef treble \global \hornSecond}}
  }
}