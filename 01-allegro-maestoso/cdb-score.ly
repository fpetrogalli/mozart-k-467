\version "2.14.0"

\include "global.ly"
\include "cdb-music.ly"

\header {
  instrument = "Cello - Double bass"
}

\score {
  \new Staff {
    \global \cdb 
  }
}