\version "2.14.0"

\parallelMusic #'(trumpetFirst trumpetSecond) {
  R1*6 |
  R1*6 |

  c4\p c8. c16 d 4 d8. d16 |
  e4\p e8. e16 g4 g8. g16 |
				%page 2
  e4 f8-. e-. d4 r |
  c4 d8-. c-. g4 r|

  R1*2 |
  R1*2 |

  c4 c8. c16 d4 g, |
  e4 e8. e16 g4 g, |

  g4\f r r2 |
  c4\f r4 r2 |

  g2 g4 g |
  g2 g4 g |

  g4 r r2 |
  g4 r r2 |


  g2 c4 d |
  g2 c4 g' |

  e4 r r2 |
  c4 r r2 |

  e2 e4 e |
  e,2 e4 e |

  e4 r r2 |
  e4 r r2 |

  d2 d4 d |
  d'2 d4 d |

  g,2 r |
  g,2 r |

  g2 r |
  g2 r |

  %%bar 22
  g2 r |
  g2 r |

  g2 r |
  g2 r |

  g4 r g r|
  g4 r g r|

  g4 g g g |
  g4 g g g |

  g4 r r2 |
  g4 r r2 |

  R1 |
  R1 |
  
  g2( e4) r  |
  e2( c4) r |

  R1*3 |
  R1*3 |

  g2( e4) r  |
  e2( c4) r  |

  e'2( c4) r |
  g'2 (e4) r |
  
  R1*8 |
  R1*8 |

  %%bar 42
  c8\f r g r c r e r |
  c8\f r g r c r e r |
 
  g1 |
  g1 |

  c,2 r |
  c,2 r |

  %%bar 45
  c2 r |
  c2 r |

  c2 r |
  c2 r |

  c2 r |
  c2 r |

  c2 r |
  c2 r |

  c4 c8. c16 c4 c |
  c4 c8. c16 c4 c |

  g4 g8. g16 g4 g  | 
  g'4 g8. g16 g4 g  | 
  
  g4 g8. g16 g4 g |
  g4 g8. g16 g4 g |

  %%bar 52
  g4 r r2 |
  c,4 r r2 |

  R1*11 |
  R1*11 |

  %%bar 64
  c8\f r g r c r e r |
  c8\f r g r c r e r |

  g4 g8. g16 g4 g |
  g4 g8. g16 g4 g |

  g2 c, |
  g2 c, |

  e 4 e8. e16 d4 d |
  c'4 c8. c16 g4 g |
  
  c4 r r2 |
  e4 r r2 |

  R1*7 |
  R1*7 |

  %%bar 76
  g1\p~  |
  g,1\p~ |
  
  g1~ |
  g1~ |

  g1~ |
  g1~ |

  g4 r r2\fermata |
  g4 r r2\fermata |

}
%%%
%%%trumpetFirst = \relative c'' {
%%%%  \key c \major
%%%%  \time 4/4
%%%  \clef treble
%%%
%%%				%page 1
%%%  R1*6 |
%%%  c4\p c8. c16 d 4 d8. d16 |
%%%				%page 2
%%%   e4 f8-. e-. d4 r |
%%%  r1 | 
%%%  r1 |
%%%  c4 c8. c16 d4 g, |
%%%  g\f r r2 |
%%%  g2 g4 g |
%%%  g r r2 |
%%%
%%%  g2 c4 d |
%%%  e r r2 |
%%%  e2 e4 e |
%%%  e r r2 |
%%%  d2 d4 d |
%%%  g,2 r |
%%%  g2 r |
%%%
%%%}
%%%
%%%trumpetSecond = \relative c' {
%%%%  \key c \major
%%%%  \time 4/4
%%%  \clef treble
%%%
%%%				%page 1
%%%  R1*6 |
%%%  e4\p e8. e16 g4 g8. g16 |
%%%				%page 2
%%%  c4 d8-. c-. g4 r|
%%%  r1 |
%%%  r1 |
%%%  e4 e8. e16 g4 g, |
%%%  c\f r4 r2 |
%%%  g2 g4 g |
%%%  g r r2
%%%
%%%  g2 c4 g' |
%%%  c r r2 |
%%%  e,2 e4 e |
%%%  e r r2 |
%%%  d'2 d4 d |
%%%  g,2 r |
%%%  g r |
%%%
%%%}