\version "2.14.0"

viola=\relative c' {
  \clef alto
				%page 1
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r |
  r8 e e e e4 r |
  r8 <g, f'>8 <g f'>8 <g f'>8 <g f'>4 r |
  R1*2 |
				%page 2
  r8 e' e e e4 r |
  r8 <g, f'>8 <g f'>8 <g f'>8 <g f'>4 r |
  R1 |
  c8\f r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |

  g4.( \times 2/3 {a16 g f} e4) r8. g16 |
  c,8 r e r a r c r |
  gis4.( \times 2/3 {a16 gis fis} e4) r8. gis16 |
  a8 r e r c' r a r |
  fis4.( \times 2/3 {g16 fis e} d4) r8. fis16 |
  g8 g g g g g g g |
  g g g g g g g g |

				%page 3
  g g g g g g g g |
  g g g g g g g g |
  g g g g g g g g |
  g g g g g g g g |
  g4 r r2 |
  R1 |
  c,4\p c c c|
  c c c c |

  g'1 |
  g, |
  c4 c c c |
  c c c c|
  g'1 |
  g, |
  c4\p r r2
  d8 r g, r d' r f r |

				%page 4
  e4.( \times 2/3 {f16 e d} c4) r |
  R1 |
  c8\f r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r |
  <g g'>2:16 <g g'>2:16 |
  <g g'>2:16 <g g'>2:16 |
  g'8 g, g g aes aes aes aes |
  g g g g aes aes aes aes |
  g g g g aes aes aes aes |
  g4:16 c4:16 g'4:16 bes:16 |
  a2.:16 aes4:16 |
  a!2:16 c2:16|
  c2:16 c,:16 |
  b:16 d:16 |

				%page 5
  c4 r r2 |
  R1 |
  c8\p c c c c c c c |
  c c c c d d d d |
  c4 r r2 |
  R1 |
  c8 c c  c c c c c |
  c c c c b-. b( c d) |
  c c c c ees ees ees (c) |
  
  c c c c c c c c |
  c c c c c c c c |
  c c c c b b b b |
  c4 r r2 |
  c8\f r g r c r e r |
  f4. d16 (b) c4. a16(fis) |
  g4 g' r g |
  g r r2 |
  r2 c,2\p( |

				%page 6
  g4) r r2 |
  e2( c) |
  d4 r r2 |
  c'4( f ees c) |
  g' r r8 d d d |
  d4 r r8 g g g |
  g4 r r2 |

  R1*2| 
  r1\fermata |
  c,8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r |
  
				%page 7
  R1*4 |
  g1~ |
  g~ |
  
  g4 r r2 |
  R1*3 |
  c,1~ |
  c( |
  d) |

				%page 8
  c4 r b r |
  c2( d) |
  e4 r r r8 g |
  a f g e f d e c |
  f4 r r2 |
  
  R1*3 |
  g4( fis f  e) |
  d( c b a) |
  g\f r r2 |
  g4. g16(a b8) b16(c d8) \times 2/3 {d16( e fis} |
  g4) r r2 |
  R1*3 |

				%page 9
  R1*9
  fis2\p\( g |
	   fis g |
	   fis4\) r r2 |
  R1*3 |
  
  r4 g\p r g |
  r e r e |
  r d r d |
  
  r d r d |
  R1*14 |
  
				%page 10
  a'8\p r d, r a' r c r |
  b4.( \times 2/3 {c16 b a} g4) r |

				%page 11
  c,8 r g r c r e r |
  R1 |
  b8 r fis r b r d r |
  R1 |
  a8 r e r a r c r |
  
  R1 |
  r2 r4 g'8 a |
  b4 r r e,8 fis |
  g4 r r c,8 d |
  e4 a,8 b c4 fis,8 g |
  a1~|
  a1~|
  a4 r r2 |
  R1*1 |

				%page 12
  R1*5|
  r8 d\p d d d d d d |
  
  R1 *4 |

  d8 d d d <c d> <c d> <c d> <c d> |
  <b d>4 r r2 |
  R1*3

 r8 d d d <c d> <c d> <c d> <c d> |
  <b d>4 r r2 |
  R1 |
  r2 r4  d8 d |

				%page 13
  b4 r r b8 b |
  g4 r r g8 g |
  a4 r r2 |
  g'1~ |

  g |
  bes,2. e8 r  |
  g r bes r cis r e r |
  d,,8  fis g a b! c! d fis |
  g4 r r2 |
  R1*2 |
  d8 d d d d d d d  |
  
				%page 14
  
}