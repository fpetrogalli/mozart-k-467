\version "2.14.0"

\include "global.ly"
\include "piano-music.ly"

\header {
  instrument = "Piano"
}

\score {
  \new PianoStaff  {
    <<
      \new Staff = "RH" {\relative c'' {\set Score.skipBars = ##t \global \pianoRH} }
      \new Staff = "LH" {\relative c {\set Score.skipBars = ##t \global \clef bass \pianoLH} }
    >>
  }
}
