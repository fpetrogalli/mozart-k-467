\version "2.14.0"

violinII=\relative c'{
  \clef treble
  % page 1
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r
  r8 g g g g4 r |
  r8 <d b'>8 <d b'>8 <d b'>8 <d b'>4 r |
  R1*2 | 
  				%page 2 
  r8 g g g g4 r |
  r8 <d b'>8 <d b'>8 <d b'>8 <d b'>4 r |
  R1 |
  e'2:16\f e: |
  d2.: e4: |
  f2: f: |
  
  e2: g4: b: |
  c2: c: |
  b: gis4: b: |
  e,: a2: c4: |
  a2: fis4: a: |
  g4.( \times 2/3 {a16 g fis} g8) g-. fis-. g-. |
  a4.( \times 2/3 {b16 a g} fis8) fis-. g-. a-. |

				%page 3
  b4.( \times 2/3 {c16 b a} g8) g-. a-. b-. |
  c4.( \times 2/3 {d16 c b} c8) c-. b-. c-. |
  d4.( cis8) d4.( cis8) |
  d8( cis8) d8( cis8) d8( cis8) d8( cis8) |
  d-. c!-.\p b-. a-. g-. f-. e-. dis-. |
  d!-. c-. b-. a-. g-. f-. e-. d-. |
  c4 r r2 | 
  r1 |

  R1*8 |

				%page 4
  c'8\p r g r c r e r |
  f4.( \times 2/3 { e16 d c } b4) r |
  c8\f r g r c r e r |
  f4.( \times 2/3 { e16 d c } b4) r |
  c8 r g r c r e r |
  f4.( \times 2/3 { e16 d c } b4) r |
  c2~ (c8 des c b) | 

  c2~ (c8 des c b) | 
  c2~ (c8 des c b) | 
  c2:16 c:16 |
  c2.:16 d4:16 |
  ees2:16 ees2:16 |
  e!2:16 e,:16 |
  d:16 f:16 |

				%page 5
  e4 r r2 |
  r1 | 
  d8\p d d d d d d d |
  e e e e f f f f |
  e4 r r2 |
  r1 |
  d8 d d d d d d d |
  e e e e f f f f |
  e e e e fis fis fis( ees')
  e! e e e, a a a a |
  d, d d d ees ees ees ees |
  e! e e e f f f f |
  e4 r r2 |
  
  c'8\f r g r c r e r |
  f4. d16( b) c4. a16 ( fis) |
  g4 <c e> r <b g'> |
  <c e> r r2 |
  r2 a2\p( |

				%page 6
  g4) r r2 |
  c,1 |
  b4 r r2 |
  ees4( b) c2 |
  d4 r r8 b b b |
  b4 r r8 d d d |
  d4 r r2 |
  r1 |
  r1 | 
  r1\fermata |
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r

				%page 7
  R1*4 |
  e2 ~e8  dis-. e-. c-.
  d!2~ d8 cis-. e-. b-.

  c!4 r r2 |
  R1*3 |
  e1( |
  f~) |
  f |

				%page 8
  e4 r g r |
  g( a2 b4) |
  c4 r r r8 d |
  c4( b a g ) |
  a4 r r2 |

  R1*3 |
  b,4( c d g) |
  f( e d c) |
  
  b4.\f (g16 a b8) b16( c d8) \times 2/3 {d16( e fis} |
  g4.) g16( a b8) b16( c d8) \times 2/3 {d16( e fis} |
  g4) r r2 |
  R1*3 |

				%page 9
  R1*9 |

  a,2(\p bes |
  a bes |
  a4) r r2 |
  r1 |

  R1*2 |
  r4 b r b |
  r a r a |
  r fis r fis |

  r g r g |
  R1*5 |

				%page 10
  R1*6 |
 
  R1*2 |
  g8\p r d r g r b r |
  
  c4.( \times 2/3 {b16 a g} fis4) r |
  r1 |
  
				%page 11
  r1 |
  fis8 r c r fis r a r |
  r1 |
  e8 r b r e r g r |
  r1 |
  
  d8 r a r d r fis r |
  g4 r r e'8 e |
  e4 r r b8 b |
  c4 r r g8 g |

  a4 e8 e fis4 c8 c |
  c1~ |
  c1~ |
  c4 r r2 |
  r1 |

				%page 12

  R1*5 |
  r8 g'\p g g g g g g |
  g g g g fis fis fis fis |
  g4 r r2 |
  R1*3 |
  
  R1*4 | 

  r8 g\p g g fis fis fis fis |
  g4 r r2 |
  r1 |
  r2 r4 b8 a |

				%page 13
  g4 r r g8 fis |
  e4 r r e8 d |
  c4 r r2 |
  b'1 |

  c |
  e,2. g8 r |
  bes r cis r d r g r |
  r1 |

  g,,8 a b c! d fis g a |
  b4 r r2 |
  r1 |
  g8 g g g g g g  g |
  
				%page 14 
  
  fis fis fis fis fis fis fis fis |
  b2:16\f b:16 |
  a2.:16 b4:16 |
  c2:16 c:16 |
  b:16 d4:16 fis:16 |
  g2:16 g:16 |
  g:16 g:16 | 
  
  aes2:16 aes:16 |
  g:16 aes:16 |
  g4:16 aes:16 g:16 aes:16 |
  g2:16 g:16 |
  b4 r <d,, c'> r |
  <g, d' b'> r r2 |
  r1 |
  a8\p a a a a a a a |
  
				%page 15
  b8 b b b c c c c |
  b4 r r2 |
  r1 |
  a8 a a a a a a a |
  b b b b c c c c |
  b b b b   cis( bes') bes-. bes-.|
  b!( b,) b-. b-. e e e e |
  a, a a a ais ais ais ais |
  
  b8 b b b b( g' dis e) |
  e( fis g fis g) g( dis e) |
  e4( g fis e) |
  dis8 fis fis fis g g g g |
  fis fis fis fis g g g g |
  fis4 r fis r |
  fis r r2 |
  r4 e4(\p g fis) |

  e4 r r2 |
  r4 fis( a g) |
  fis r r 2|
  R1*4 | 
  
				%page 16
  r1 |
  e1\p |
  r1|
  dis'1( |
  e8) g, g g  r fis fis fis  |
  
  r8 e e e r dis dis dis |
  e4 r r2 |
  r1 |
  r4 b r d! |
  r gis b d |
  
				%page 17
  
  dis4 r r2 |
  r1 |
  r4 e, r g! |
  r cis e b |
  fis r r2 |

  r1 |
  r4 a, r c! |
  r fis a c |
  b! r r2 |
  r1 |

				%page 18 
  r2 des,( |
  c4) r c2( |
  f4) r f( ees |
  d bes  c d) |

  ees2.( d4 |
  c aes bes c) |
  d2.( c4 |
  b! g a! b) |

  c1~ |
  c1~ |
  c1~ |
  c1~ |
  c1~ |

				%page 19
  c4 r r2 |
  r1 |
  b'1( |
  c) |
  b4 r r2 |

  r1 |
  r8 b b b r a a a |
  r g g g r f! f f |
  r e! e e r d d d |
  r c c c  r b b b |

				%page 20
  c8\p r g r c r e r |
  f4. (\times 2/3 {e16  d c } b4) r8. g16 |
  b8 r d r f r d r |
  g4. ( \times 2/3 {a16 g f } e4) r |
  r8 g g g g4 r |
  r8 <b, g'> <b g'> <b g'> <b g'>4 r4 |
  r1 |

  r1 |
  r8 g' g g g4 r4 |
  r8 <b, g'> <b g'> <b g'> <b g'>4 r4 |
  r1 |
  e'2:16\f e:16 |
  d2.:16 e4:16 |

				%page 21

  f2:16 f:16 |
  e:16 g4:16 b:16 |
  c2:16 c:16 |
  b:16 gis4:16 b:16 |
  e,4:16 a2:16  c4:16 |
  a2:16 fis4:16 a:16 |

  d,4:16 g2:16 bes4:16 |
  g2:16 e4:16 g:16 |
  f!4 r r2 |
  g,8\p r c, r g' r bes r |
  a4.( \times 2/3 {bes16 a g} f4) r |
  r1 |
  
				%page 22
  R1*6 |

  d'2( ees) |
  d( ees) |
  d4 r r2 |
  R1*2 |
  
  R1*3 |
  
  r4 e,\p r e  |
  r d r d |
  
				%page 23
  r4 b r b |
  r c r c |
  R1*4 |

  R1*7 |
  
  R1*2 |
  
  c8\p r g r c r e r |
  f4.(\times 2/3 {e16 d c } b4) r |
  r1 |
  
				%page 24
  R1 |
  b'8\p r f r b r d r |
  r1 | 
  a8 r e r a r c r |
  
  r1 |
  g8 r d r g r b r |
  c4 r r g8 g |
  a4 r r e'8 e |

  f4 r r c8 c |
  d4 a8 a b4 f8 f |
  f1~ |
  f~ |
  
				%page 25 
  f4 r r2|
  r1 |
  d4.( cis8 d cis d cis) |
  d4 d'8( cis d cis d cis) |

  d8-. c!-. b-. a-. g-. f-. e-. dis-. |
  d!4 r r 2|
  R1*2 |
  b2.( c4 |
  d b g b |
  
				%page 26
  c4 r r2 |
  R1*5 |

  R1*4 |
  r8 c\p c c  b b b b |
  c4 r r e8 d | 
  c4 r r c'8 b |
  a4 r r a8 g |
  
				%page 27
  f4 r r2 |
  e1(  |
  g |
  f) |
  c2. ees8 r |
  
				%page 28 


  a8 r c r ees ees ees ees |
  g,4 r r2 |
  R1*3 |

  r1 |
  r8 c, c c c c c c |

  r8 c c c c c c c |
  r8 c c c c c c c |
  
  b b b b b b b b |
  e'2:16\f e:16 |
  d2.:16 e4:16 |
  f2:16 f:16 |
  e:16 g4:16 b:16 |
  c2:16 c:16 |
  
  c16 c, c c c2.:16 | des2:16 des:16 |
  c8 c'4 c8( des8) des4 des8( |
  c) c, des des c c des des |
  c2:16 c:16 |
  b!4 r <d g> r |
  <c g'> r <ees, c'> r |
%  <g, e'! c'>4 r r2\fermata |
  << 
    {\stemDown c'2} \\
    {<g, e'!>4 s4}
  >> r2\fermata | 
 
  c'2\f(~ c8 des c b) |

  c2\f(~ c8 des c b) |
  c2\f(~ c8 des c b) |
  c2:16 c:16 c2.:16 d4:16 |
  ees2:16 ees:16 |
  e!4:16 c:16 g:16 e:16 |
  d2:16 f:16 |
  
				%page 30
  e4 r r2 |
  r1 |
  d8\p d d d d d d d |
  e e e e f f f f |
  e4 r r2 |
  
  c'8\f r g r c r e r |
  f4. d16(b) c4. a16( fis) |
  
  g4 <c e> r <b g'> |
  <c e > r r r8. g,16\p |
  c8 r e r g4 r8. g,16 |
  c8 r e r g4.( \times 2/3 {f16 e d}|
  c8) r g r c r e r |
  c4 r r2 \bar "|."

}