\version "2.14.0"

\parallelMusic #'(pianoRH pianoLH) {

%%%piano music starts at bar 74
  R1*73 |
  R1*73 |
  
				%74
  g16( a g fis g d b' a g4) r |
  <g g'>4 r r2 |
  
				%75
  b16( c b a b g d' c b4) r |
  <g g'>4 r r2 |
  
				%76
  d16\( e d cis d b f' e f g f e f d b' a |
  r1 |
	
				%77
  b16 c b a b g d' cis d e d cis  d b f' e |
  r1 |
	
				%78
  f16 d b d f d f d f d b d f d f d |
  <g' b d>2 <g b d>2 |
	
				%79
  f1\fermata\) |
  <g, b d g>1\fermata |
  
  				%80
  g,1\startTrillSpan |
  r1 |
  
  				%81
  g1 |
  r1 |

  				%82
  g1 |
  r1 |
  
  				%83
  \afterGrace g1 {fis32 [g]\stopTrillSpan} |
  r1 |

  				%84
  g2~ (g8 fis g e) |
  \clef treble r8 <c' e g> <c e g> <c e g> <c e g>4 r |
  
  				%85
  f!2~ (f8 e f d) |
  r8 <b d g> <b d g> <b d g>  <b d g>4 r|
  
  				%86
  c16 ( d c b c8) cis d16 (e d cis d8) dis |
  <c e g>4 r <b f' g> r |

  				%87
  e8 ( f16 g f8) e d! \( e f fis |
  <c e g>2 <g b g'>4 r |

  				%88
  g8. e16\) e'4~ e16 d c b a g f e |
  r8 <c e g> <c e g> <c e g> <c e g>4 r |

  				%89
  g16 f d' b f'4~ f16 e d c b a g f |
  r8 <b d g> <b d g> <b d g>  <b d g>4 r|

  				%90
  e16 g c b a g f e d fis g f e d c b |
  \clef bass <c, c'>4 r <g g'> r |

  				%91
  c4\) r r8 c\turn (e c |
  \clef treble e'' (c e c e c g' e) | 

  				%92
  gis8 a) a4. a8 ( f' d |
  f8_\markup{"legato"} c f c f c f c |

  				%93
  c8 b) b4. b8 (a' g) |
  d8 c d c d c f c |

  				%94
  g8. (e16 c4) r8 c\turn (e c |
  e8 c e c e c g' e |

  				%95
  b8. a16) a'4~ (a16 g f e d c b a |
  f8 c f c f c f c |

  				%96
  c8. b16) b'4~_\markup{"legato"} b16 a g f e d c b |
  d8 c d c d c f c |

  				%97
  d16 c c' b a g f e cis d d' c! b a g f |
  e8 c e c b g' f g |

  				%98
  dis16 e e' d! c b a g e f f' e d c b a|
  c,8 g' e a d, a' f b |

  				%99
  fis16 g e' e, g f d' d, f e c' c, e d b' b, |
  e,8 c' d b c a b g |

  				%100
  d16 c a' a, c b g' g, b a f' f, a g e' e, |
  a8 f g e f d e c |

  				%101
  r16 d e f g a b c cis d dis e f fis g gis |
  \clef bass f,4 r r2 |

  				%102
  b16 a c b a g! f! e g f a g f e d! c! |
  f4. g8 a4. fis8 |

  				%103
  <b g'>4 r8 <c a'> <d b'>4 r8 <e c'> |
  \clef treble g16_\markup{"legato"} g' fis g fis g fis g f g f g e g e g |

  %104
  <<{c'8 ([b])}\\{f4}>> r8 <<{a8 a ([g])}\\{e8 d4}>> r8 <<{fis8}\\{c8}>> |
  d16 g d g c, g' c, g'  b, g' b, g' a, g' a, g'|

  %105
  b16 \(g' a g c, a' b a d, b' c b e, c' d c |
  g,16 g' fis g fis g fis g f g f g e g e g |
  
  %106%
  f,16 b c b e, a b a d, g a g c, fis g fis\) |
  d16 g d g c, g' c, g'  b, g' b, g' a, g' a, g'|

  				%107
  <b, d g>4\f r r2 |
  g,4 r r2 |
} 

%%%pianoRH = {
%%%
%%%}
%%%
%%%pianoRH = {
%%%
%%%}