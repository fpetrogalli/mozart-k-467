\version "2.14.0"

violinI=\relative c'{
  \clef treble
  % page 1
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r
  g'2~ g8 fis-. g-. e-. |
  f?2~ f8 e-. f-. d-. |
  c4 r r2 |
				%page 2
  r2 d8( e f fis) |
  g2~  g8( \grace a32 g16 fis g8) e-. |
  f?2~ f8( \grace g32 f16 e f8) d-. |
  c4 r r2 |
  c'2:16\f c: |
  c4: a: b: cis: |
  d2: d2: |
  
  d4: b: c!: d: |
  e2: e: |
  e4: dis: e: d: |
  c2: ees: |
  ees4: cis: d: c: |
  b4.( \times 2/3 {c16 b a} g8) g-. a-. b-. |
  c4.( \times 2/3 {d16 c b} a8) a-. b-. c-. |

  %%bar 22
  d4.( \times 2/3 {e16 d c} b8) b-. c-. d-. |
  e4.( \times 2/3 {f16 e d} c8) c-. d-. e-. |
  f4.( e8) f4.( e8) |
  f8 (e) f8 (e) f8 (e) f8 (e)  |
  f-. e-. d-. c-. b-. a-. g-. fis-. |
  f!-. e-. d-. c-. b-. a-. g-. f-.  |
  e4 r r2 |
  R1*7 |
  
  %%bar 36
  c'8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r |
  
  %%bar 38
  g'8 g4 g g g8~ | 
  g8 g4 g g g8  |  
  g2:16\f g2: |
  g2: g2: |
  c8 r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r |
  c2~( c8 des  c b) |

  %%br 45
  c2~( c8 des  c b) |
  c2~( c8 des  c b) |
  c8-. g-. g2 gis4~( |
  gis8 a) a2 b4~( |
  b8 c) c2 fis,4 |
  g8 g g4. a16 g f! e d c |
  f8 f f4. g16 f e d c b |

  %%bar 52
  c4 r r2 |
  R1 |
  a'2~\p( a8 c) b-. a-. |
  a8( g) f-. e-. e( d) c-. b-. |
  c4 r r2 |
  R1 |
  a'2~ a8 c b-. a-. |
  a8( g) f-. e-. e( d) c-. b-. |
  d( c) c-. c-. c'4. (fis,8) |
 
  %%bar 61
  g4.( c,8) e4( d) |
  a'2~ a8 a-. b-. c-. 
  \grace c32( c,2) d4.\trill( c16 d) | 
  c4 r r2 |
  c8\f r g r c r e r |
  f4. d16( b) c4. a16( fis) |
  g4 <c' e,> r  <b d,> |
  < c e,> r r2 |
  r2 c,2\p( |

  %%bar 
  b4) r r2 |
  R1*2 |
  ees4( b) c2 |
  b4 r r8 g g g |
  g4 r r8 b b b |
  b4 r r2 |
  R1*2 |
  r1\fermata |
  c,8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r
   
				%page 7
  R1*4 |
  g'2~ g8 fis-. g-. e-. |
  f?2~ f8 e-. f-. d-. |
  %%
  c4 r r2 | R1*3 |
  g1( | a | b) |

				%page 8
  c4 r d r | e2( f) | g4 r r r8 b8~( |
  b a4 g f e8) | d4 r r2 |
  %%
  R1*3 |g,4( a b) c~ |
  %%
  c8 b4( a g fis8) |
  < g, g'>4.\f g16( a b8) b16( c d8) \times 2/3 {d16( e fis} |
  g4.) g16( a b8) b16( c d8) \times 2/3 {d16( e fis} |
  g4) r r2 | R1*3 |

				%page 9
  R1*9|
  %%
  d2\p( cis | d cis | d4) r r2 | R1 |
  %%
  R1*2 | r4 d\p  r d | r c r c| r a r a |
  %%
  r d r d | R1*5 |

				%page 10
  R1*6 |
  %%
  R1*2 |
  g8\p r d r g r b r |
  c4.( \times 2/3 {b16 a g} fis4) r | R1 |

				%page 11
  R1 | fis8 r c r fis r a r |
  R1 | e8 r b r e r g r | R1 |
  %%
  d8 r a r d r fis r |
  g4 r r b8 a | g4 r r g8 fis | e4 r r e8 d |
  %%
  c4 c8 b a4 a8 g |
  fis4.( g8 fis g fis g) |
  fis4.( g8 fis g fis g) |
  fis4 r r2 | R1 |

				%page 12
  R1*5 | r8 b\p b b  b b b b|
  %%
  b b b b a a a a | g4 r r2 | R1*3 |
  %%
  R1*4 |
  %%
  r8 b\p b b a a a a | g4 r r2 | R1 | r2 r4 b'8 a |

				%page 13
  g4 r r g8 fis | e4 r r e8 d | c4 r r2 | d1 |
  %%
  e1 | g,2. bes8 r | cis r e r g r bes r | R1 |
  %%
  g,8 a b? c d fis g a | b4 r r2 | R1 | b,8 b b b b b b b |

				%page 14
  a8 a a a a a a a |
  g'2:16\f g:16 | g4:16 e:16 fis:16 gis:16 |
  a2:16 a:16 | a4:16 fis:16 g?:16 a:16 |
  bes2:16 bes:16 | bes2:16 bes:16 |
  %%
  bes2:16 bes:16 | bes2:16 bes:16 |
  bes2:16 bes:16 | bes4:16 b:16 c:16 cis:16|
  d r <d,, a' fis'>4 r | 
  <d b' g'> r r2 | R1 |
  e'2\p~( e8 g) fis-. e-. |

				%page 15
  e8( d) c-. b-. b( a) g-. fis-. |
  g4 r r2 | 
  R1 |
  e'2\p~( e8 g) fis-. e-. |
  e8( d) c-. b-. b( a) g-. fis-. |
  a8( g) g-. g-. g'4.( cis,8) |
  d4.( g,8) b4( a) |
  e'2~ e8 e-. fis-. g-. |
  %%
  g8( dis e4.) e8-. fis-. g-. |
  g8( dis e4.) e8-. fis-. g-. |
  g4( ais,2) ais4 |
  b4 r e4.( ais,8) |
  b4 r g'4.( ais,8) |
  b4 r b r |
  b r r2 |
  r4 g\p( b a) |
  %%
  g4 r r2 | r4 a( c b) | a r r2 | R1*4 |

				%page 16
  R1 |
  g1\p |
  R1 |
  dis'1 ( |
  e8) b b b r a a a |
  %%
  r8 g g g r fis fis fis |
  e4 r r2 |
  R1 |
  r4 d? r gis |
  r b d gis |

				%page 17
  a r r2 |
  R1 |
  r4 g,? r cis |
  r e g cis | d r r2 |
  %%
  R1 |
  r4 c,,? r fis |
  r a c fis |
  g r r2 |
  R1 |

				%page 18 
  r2 des,( |
  c4) r c2 ( |
  f4) c aes'2~ |
  aes4 d,4( ees f) |
  %%
  g1~|
  g4 c,( d ees) |
  f1~ |
  f4 b,( c d) |
  %%
  r8 ees( d ees) r8 e( d e) |
  r f(e f ) r g( f g ) |
  r aes( g aes) r a (g a) |
  r bes (a bes) r b( a b ) |
  r c( b c) r d( c d) |

				%page 19
  ees4 r r2 | R1 | e1( | ees) | d4 r r2 |
  %%
  R1 |
  r8 d d d r c c c |
  r b b b r a a a |
  r g g g r f? f f |
  r e? e e r d d d |

				%page 20
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r4 |
  g'2~ g8 fis-. g-. e-. |
  f?2~ f8 e-. f-. d-. |
  c4 r r2 |
  %%
  r2 d8( e f fis) |
  g2~ g8 \grace a32 g16( fis g8) e-. |
  f?2~ f8 \grace g32 f16( e f8) d-. |
  c4 r r2 |
  c'2:16\f c:16 |
  c4:16 a:16 b:16 cis:16 |

				%page 21
  d2:16 d:16 |
  d4:16 b:16 c?:16 d:16 |
  e2:16 e:16 |
  e4:16 dis:16 e:16 d:16 |
  c2:16 ees:16 |
  ees4:16 cis:16 d:16 c:16 |
  %%
  bes2:16 des:16 |
  des4:16 b?:16 c:16 bes:16 |
  a8 r c,\p r f r a r |
  bes4.( \times 2/3 {a16 g f} e4) r |
  R1*2 |

				%page 22
  R1*6 |
  %%
  g2\p( fis) |
  g2( fis) |
  g4 r r2 |
  R1*2 |
  %%
  R1*3 |
  r4 g,\p r g |
  r f r f |
  
				%page 23 
  r4 d r d |
  r g r g |
  R1*4 |
  %%
  R1*7 |
  %%
  R1*2 |
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r |
  R1 |
  
				%page 24
  R1 |
  b'8\p r f r b r d r |
  R1 |
  a8 r e r a r c r |
  %%
  R1 |
  g8 r d r g r b r |
  c4 r r e,8 d |
  c4 r r c'8 b|
  %%
  a4 r r a8 g |
  f4 f8 e d4 d8 c |
  b4.( c8 b c b c) |
  b4.( c8 b c b c) |

				%page 25
  b4 r r2 |
  R1 |
  f4.( e8 f e f e ) |
  f4 f'8( e f e f e) |
  %%
  f8-. e-. d-. c-. b-. a-. g-. fis-. |
  f?4 r r2 |
  R1*2 |
  d2.( e4 |
  f d b d ) |
  
				%page 26
  c4 r r2 |
  R1*5 |
  %%
  R1*4 |
  r8 e\p e e d d d d |
  c4 r r e'8 d|
  c4 r r c'8 b |
  a4 r r a8 g |

				%page 27
  f4 r r2 |
  c1( |
  bes |
  a) |
  ees2. a8 r |
  %%
  c8 r ees r fis c' c c |
  g,4 r r2 |
  R1*3 |

				%page 28
  R1 |
  r8 e e e e e e e |
  r8 e e e e e e e |
  r8 d d d d d d d |
  %%
  r8 d d d d d d d |
  c''2:16\f c:16 |
  c4:16 a:16 b:16 cis:16 |
  d2:16  d:16 |
  d4:16 b:16 c?:16 d:16 |
  ees2:16 ees:16 |

				%page 29
  ees16 ees, ees ees ees2.:16 |
  ees2:16 ees:16
  ees8 ees'4 ees ees ees8~ |
  ees8 ees,4 ees ees ees8 |
  ees4:16 e:16 f:16 fis:16 |
  g4 r b r |
  c r fis, r |
  << 
    {\stemDown g2} \\
    {<e,? c'>4 s4}
  >> r2\fermata | 
  %%  <e,? c' g'>2 r2\fermata | 
  c''2~(\f c8 des c b) |
  %%
  c2~( c8 des c b) |
  c2~( c8 des c b) |
  c8-. g-. g2 gis4~ |
  gis8 a-. a2 b4~ |
  b8 c c2 fis,4 |
  g8 g g4. a16 g f? e d c |
  f8 f f4. g16 f e d c b |

				%page 30
  c4 r r2 | R1 |
  a'2~(\p a8 c) b-. a-. |
  a( g) f-. e-. e( d) c-. b-. |
  c4 r r2 |
  c8\f r g r c r e r |
  f4. d16( b) c4. a16( fis) |
  %%
  g4 <e' c'> r < d b'> |
  <e c'> r r r8. g,,16\p |
  c8 r e r g4 r8. g,16 |
  c8 r e r g4.( \times 2/3 {f16 e d} |
  c8) r g r c r e r |
  g4 r r2 \bar "|."
}