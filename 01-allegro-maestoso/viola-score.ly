\version "2.14.0"


\include "global.ly"

\include "viola-music.ly"
\header {
  instrument = "Viola"
}

\score {
  \new Staff {
    \global \viola
  }
}