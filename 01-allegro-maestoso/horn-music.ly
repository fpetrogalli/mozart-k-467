\version "2.14.0"

\parallelMusic #'(hornFirst hornSecond) {
  R1*6 |
  R1*6 |

  c4\p c8. c16 d4 d8. d16 | 
  e4\p e8. e16 g4 g8. g16 |
				%page 2
  e4 f8-. e-. d4 r |
  c4 d8-. c-. g4 r|

  R1*2 |
  R1*2 |

  c4 c8. c16 d4 g, |
  e4 e8. e16 g4 g, |

  g4\f r r2 |
  c4\f r4 r2 |

  g2 g4 g |
  g2 g4 g |

  g4 r r2 |
  g4 r r2 |

  

  g2 c4 d |
  g2 c4 g' |

  e4 r r2 |
  c4 r r2 |

  e2 e4 e |
  e,2 e4 e |

  e4 r r2 |
  e4 r r2 |

  d2 d4 d |
  d'2 d4 d |

  g,1 ~ |
  g,,1~ |

  g1~ |
  g1~ |

  %%bar 22
  g1~ |
  g1~ |

  g1 |
  g1 |

  g2 g |
  g2 g |

  g4 g g g |
  g4 g g g |

  g1\fp~ |
  g1\fp~ |

  g1 |
  g1 |
  
  g2( e4) r  |
  e'2( c4) r |

  R1*3 |
  R1*3 |

  g2( e4) r  |
  e2( c4) r  |

  e'2( c4) r |
  g'2 (e4) r |
  
  R1*6 |
  R1*6 |

  %%bar 40
  g1\f~ |
  g,1\f~ |

  g1 |
  g1 |
  
  c8\f r g r c r e r |
  c8\f r g r c r e r |
 
  g1 |
  g1 |

  c,2 r |
  c,2 r |

  %%bar 45
  c2 r |
  c2 r |

  c2 r |
  c2 r |

  c1 |
  c1 |

  c2 r |
  c2 r |

  c4 c8. c16 c4 c |
  c4 c8. c16 c4 c |

  e4 e8. e16 e4 e  | 
  c'4 c8. c16 c4 c |
  
  d4 d8. d16 d4 d |
  g4 g8. g16 g4 g |

  %%bar 52
  c4 r r2 |
  e4 r r2 |

  R1*11 |
  R1*11 |

  %%bar 64
  c8\f r g r c r e r |
  c8\f r g r c r e r |

  g4 g8. g16 g4 g |
  g4 g8. g16 g4 g |

  g2 c, |
  g2 c, |

  e 4 e8. e16 d4 d |
  c'4 c8. c16 g4 g |
  
  c4 r r2 |
  e4 r r2 |

  R1*7 |
  R1*7 |

  %%bar 76
  d1\p~  |
  g,1\p~ |
  
  d1~ |
  g1~ |

  d1~ |
  g1~ |

  d4 r r2\fermata |
  g4 r r2\fermata |

  R1*4 |
  R1*4 |

				%page 7
  R1*6 |
  R1*6 |

  c4\p c8. c16 d4 g, |
  e'4\p e8. e16 g4 g, |

  g4 r r2 |
  c4 r r2 |

  R1*5 |
  R1*5 |
  
				%page 8
  R1*5 |
  R1*5 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1 |
  R1 |

  g2\f r |
  b2\f r |

  d'2 d4 d4 |
  g'2 g4 g |

  d4 r r2 |
  g4 r r2 |

  R1*3 |
  R1*3 |

				%page 9
  R1*9 |
  R1*9 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1*5 |
  R1*5 |
  %%
  R1*6 |
  R1*6 |

				%page 10
  R1*6 |
  R1*6 |
  %%
  R1*5 |
  R1*5 |

				%page 11

  R1*5 |
  R1*5 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1*5 |
  R1*5 |

				%page 12
  r2 r4 g\f |
  r2 r4 g\f |
 
  g4 r r2 |
  g4 r r2 |

  R1*4 |
  R1*4 |
  %%
  R1*5 |
  R1*5 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |

				%page 13
  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |
  
				%page 14
  R1 |
  R1 |

  d4\f r r2 |
  g4\f r r2 |

  d2 d4 d |
  d'2 d4 d |

  d4 r r2 |
  d4 r r2 |

  d2 d4 d |
  d2 g,4 d' |
  
  d4 r r2 |
  g,4 r r2 |

  g,4 r r2 |
  g,4 r r2 |
  %%
  R1*3 |
  R1*3 |

  g4 g8. g16 g4 g |
  g'4 g8. g16 g4 g |

  d'4 r d r |
  g4 r d' r |

  d4 r r2 |
  g,4 r r2 |

  R1*2 |
  R1*2 |

				%page 15
  R1*8 |
  R1*8 |
  %%
  R1*8 |
  R1*8 |
  %%
  R1*7 |
  R1*7 |
  
				%page 16  
  R1*5 |
  R1*5 |
  %%
  R1*5 |
  R1*5 |
 
				%page 17  
  R1*5 |
  R1*5 |
  %%
  R1*5 |
  R1*5 |

				%page 18
  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1*5 |
  R1*5 |

				%page 19
  R1*4 |
  R1*4 |
  
  g,1~ |
  g,1~ |
  %%
  g1~ |
  g1~ |

  g1~ |
  g1~ |

  g1~ |
  g1~ |

  g1~ |
  g1~ |

  g1 |
  g1 |
  
				%PAGE 20
  c,4 r r2 |
  c4 r r2 |
  
  R1*5 |
  R1*5 |
  %%
  c'4\p c8. c16 d4 d8. d16 |
  e4\p e8. e16 g4 g8. g16 |
  %%
  e4 f8-. e-. d4 r |
  c4 d8-. c-. g4 r |

  g2.( e4) |
  e'2.( c4) |

  f2.( d4) |
  d2.( g,4) |

  c4 c8. c16 d4 g, |
  e4 e8. e16 g4 g, |

  g4\f r r2 |
  c4\f r r2 |

  g2 g4 g |
  g2 g4 g |
  
				%page 21
  g4 g g2 |
  g4 g g2 |

  g2 c4 d |
  g2 c4 g' |

  e4 r r2 |
  c4 r r2 |

  e2 e4 e |
  e,2 e4 e |

  e4 r r2 |
  e4 r r2 |
  
  d2 d4 d |
  d'2 d4 d |
  %%
  d4 r r2 |
  g,4 r r2 |

  c2 c4 c |
  c,2 c4 c |

  c4 r r2 |
  c4 r r2 |

  R1*3 |
  R1*3 |

				%page 22
  R1*6 |
  R1*6 |
  %%
  R1*4 |
  R1*4 |
  
  g1\p~ |
  g1\p~ |
  %%
  g1~ |
  g1~ |
 
  g1~ |
  g1~ |
 
  g1~ |
  g1 |

  g4 r r2 |
  c4 r r2 | 

  R1 |
  R1 |
  
				%PAGE 23
  R1*6 |
  R1*6 |
  %%
  R1*7 |
  R1*7 |
  %%
  R1*5 |
  R1*5 |
  
				%page 24
  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |
  
				%page 25
  R1*4 |
  R1*4 |
  %%
  g1\p~ |
  g1\p~ |

  g1 |
  g1 |
 
  g2 (e4) r |
  e'2 ( c4) r |

  R1 |
  R1 |
  
  g1~ |
  g1~ |

  g1~ |
  g1 (|

				%page 26
  g4 r r2 |
  c4) r r2 |

  g'2( e4) r |
  e'2( c4) r |

  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |
  
				%page 27
  R1*5 |
  R1*5 |
  %%
  R1 |
  R1 |

  g1~ |
  g1~ |

  g1~ |
  g1~ |

  g1~ |
  g1~ |

  g4 r r2 |
  g4 r r2 |
  
				%page 28
  R1*2 |
  R1*2 |
  
  g2( e4 c) |
  g2( e4 c) |

  g1~ |
  g1~ |
  %%
  g1 |
  g1 |
  
  g4\f r r2 |
  c4\f r r2 |

  g2 g4 g |
  g2 g4 g |
  
  g4 r r2 |
  g4 r r2 |

  g2 c4 d |
  g2 c4 g' |

  c4 r r2 |
  c,4 r r2 |
  
				%page 29
  c4 r r2 |
  c4 r r2 |
  
  R1*3 |
  R1*3 |
  
  c4 c8. c16 c4 c | 
  c4 c8. c16 c4 c | 

  d4 r d r |
  g'4 r g r |

  e r c r |
  c r c, r |

  g2 r2\fermata |
  g2 r2\fermata |

  c2\f r|
  c2\f r|
  %%
  c2 r|
  c2 r|

  c2 r|
  c2 r|

  c1~|
  c1~|
  
  c4 r r2|
  c4 r r2|

  c4 c8. c16 c4 c | 
  c4 c8. c16 c4 c | 

  e4 e8. e16 e4 e |
  c'4 c8. c16 c4 c | 
  
  d4 d8. d16 d4 d |
  g4 g8. g16 g4 g |
  
				%page 30
  c4 r r2 |
  e4 r r2 |

  R1*3 |
  R1*3 |

  c8\f r g r c r e r | 
  c8\f r g r c r e r | 
  
  g4 g8. g16 g4 g |
  g4 g8. g16 g4 g |
  
  g2 c, |
  g2 c,  |
  %%
  g4 e' r d |
  g4 c' r g |

  c4 r r2 |
  e4 r r2 |

  c,8\p r e r g r r4 |
  c8\p r e r g r r4 |

  c,8 r e r g r r4 |
  c,8 r e r g r r4 |

  c,8 r g r c r e r | 
  c,8 r g r c r e r | 

  c4 r r2 |
  c4 r r2 \bar "|."
}

