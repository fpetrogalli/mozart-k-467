\version "2.14.0"
fluteMusic = \relative c''' {
%  \time 4/4
%  \key c \major
  \clef treble
  
  R1*6 |
  c4\p c8. c16 d4 d8. d16 | 
				% page 2
  
  e4 f8-. e-. d4 r |
  R1*2           |
  c4 c8. c16 d8.-\trill c16 d8. e16 |
  c1~ |
  c4 a b cis |
  d1~ |  
  
  %% bar 15
  d4 b c! d |
  e1~ |
  e4 dis e d |
  c2 ees~
  ees4 cis d c |
  b4.( \times 2/3 {c16 b a} g8) g-. a-. b-. |
  c4.( \times 2/3 {d16 c b} a8) a-. b-. c-. |   
				%page 3
  

  %% bar 22
  d4.( \times 2/3 {e16 d c} b8) b-. c-. d-. |
  e4.( \times 2/3 {f16 e d} c8) c-. d-. e-. |
  f4. ( e8) f4. ( e8)                       |
  f8 ( e) f8 ( e) f8 ( e) f8 ( e)           |
  f4 r r2                                   |
  f8-. e-. d-. c-. b-. a-. g-. f-.          |
  e4 r r2                                   |
  c'8-. \grace d32^( c16  b c8) d-. e4-. r8 g, |
 
  %% bar 30
  g( f' e d c b a g) |
  fis( g) r4 r2      |
  r1                 |
  c8-. \grace d32^( c16  b c8) d-. e4-. r8 g, |
  g( f' e d c b a g) |
  fis( g) r4 r2      |
  R1*4               |
				%page 4
    
  %%bar 40
  R1*2               |
  c8\f r g r c r e r |
  f4.( \times 2/3 {e16 d c } b4) r |
  c2~( c8 des c b)   |
 
  %%bar 45
  c2~( c8 des c b)   |
  c2~( c8 des c b)   |
  c1~                |
  c2. d!4~(          |
  d8 ees) ees2 fis4  |
  g2 e!4 c           |
  f2 d4 b            |
				%page5
  
  %% bar 52
  c8 r cis,4( d e  |
  f fis g gis      |
  a) r r2          |
  r1               |
  c,4( cis d e     |
  f fis g gis      |
  a) r r2          |
  R1*4             |
   
  %%bar 63
  g2 b4.\trill( a16 b) |
  c8\f r g r c r e r   |
  g1                   |
  f4. d16( b) c4. a16( fis) |
  g4 e' r d            |
  c r r2               |
  R1                   |
				%page 6
  %%bar 72
  
  R1*2 |
  r4 r8 g\p fis( g a b) |
  c4( d ees fis)        |
  g r r2                | %%begin piano music
  r1                         |
  d,2\p^\markup{"Solo"} ( f! |
 
  %% bar 77
  b d)                       |
  f4( d b g)                 |
  f r r2\fermata             |
  R1*4 |
 
				%page 7
  
  R1*6 | 
  c'4\p c8. c16 d8.\trill c16 d8. e16 |
  c4 r r2 |
  R1*5| 
  
				%page 8
  
  R1*5|

  R1*4 |
  
  R1|
  g,4.\f^\markup {"Tutti"} g16( a b8) b16( c d8) 
  \times 2/3 {d16( e fis} |
  g2) b4 d |
  g^\markup{"Solo"} r r2|
  R1*3 |

				%page 9
  
  R1*9 |
 
  R1*4|
  
  R1*5|
 
  R1*5 |
  d8(\p b a g) d' (b a g) |

				%page 10
  
  e'( c b a) a2 |
  c8 ( a g fis) c'( a g fis) |
  d'( b a g) g2 |
  R1 |
  r4 d'4( ees d) |
  R1 |
  
  R1*5 |
				%page 11
  
  e1\p~ | e | d~ | d | c~ |
  c | b4 r r2 | R1*2 |
  R1*5 |

				%page 12
  
  r4 fis\f g f | e r r2 | R1*3 |
  R1*5 |
  R1*5 |
  R1*4 |

				%page 13 `
  
  R1*4 |
  R1*2 |
  r8 g' e cis bes g e cis | d4 r r2 |
  R1 | b'!1~ | b~ | b4 r r2 |

				%page 14
  
  R1 |
  %%
  g1~^\markup "Tutti" | g4 e fis gis | 
  a1~ | a4 fis g! a | bes1~ | bes~ |
  %%
  bes~ | bes~ | bes | g' | 
  d4 r fis, r | g( gis\p a b! | c cis d dis) | e r r2 |

				%page 15
  
  R1 | g,4( gis a b | c cis d dis) | e r r2 | R1*4 |
  %%
  R1*3 | b2(\p ais) | b( ais) | b4 r b r | b r r2 | R1 |
  %%
  % r4^\markup "Solo" r8. b,16( b'4) b-. |
  % c4.( \times 2/3 {b16 a g} fis4) r |
  % r r8. c16( c'4) c-. |
  % r8 ais( b g) r gis( a fis) |
  % r fis( g! e)  r eis( fis dis |
  % e?) b( ais b c cis d? dis |
  % e f! fis g\times 2/3 { gis4) a ais} |
  R1*7
				%page 16
  
  b2(\p g4) r | r e( g b) |
  c2( a4) r | r fis( a c) |
  b2( a |
  %%
  g fis) | gis4 r d'8( c b d) |
  gis,4 r d'8( c b d) |
  gis,4 r r2 | R1 |
				%page 17
  
  R1*4 | r2 c?8( bes a c) |
  %%
  fis,4 r c'8( bes a c) | fis,4 r r2 | R1*3 |
  
				%page 18
  r2 bes8( aes g bes) | e,4 r bes'8( aes g bes) |
  aes1~ | aes |
  %%
  g~ | g | f~ | f|
  %%
  ees2( e | f g | aes a | bes b | c d |

				%page 19
  ees4 ) r r2 | R1*3 | d1( |
  %%
  ees1) | d2( c | b a) | g'( f | e d ) |
  
				%page 20
  c4 r-\markup "Tutti" r2 | R1*5 |
  c4\p c8. c16 d4 d8. d16 |
  %%
  e4 f8. e16 d4 r | R1*2 | c4 c8. c16 d8.\trill c16 d8. e16 |
  c1~\f | c4 a b cis |

				%page 21
  d1~ | d4 b c? d | e1~ |
  e4 dis e d | c2 ees~ | ees4 cis d c |
  %%
  bes2 des~ | des4 b? c bes | a r r2| R1-\markup "Solo" |R1*2|

				%page 22
  f8\p r c r f r aes r |
  des2 e,! | f( e | f e) | f4 r r2| R1 |
  %%
  R1*5|
  R1 | f'1~\p | f2( d4 b) | c r r2 | R1 |

				%page 23
  R1*6 |
  %%
  R1*2| f8\p( d c b) f'8( d c b) |
  g'( e d c)  c2 | R1 | r4 g( aes g)| R1 |
  %%
  R1*4 | c8\p r g r c r e r |

				%page 24
  R1 | f1~ | f | e~ |
  %%
  e| d | c4 r r2 | R1 |
  %%
  R1*4 |
  
				%page 25
  R1*4 |
  %%
  R1 | f8-.\p e-. d-. c-. b-. a-. g-. f-. |
  e4-\markup "Tutti" r r2|
  c'8-. \grace d32^( c16 b c8) d-. e4 r8 g, |
  g( f' e d c b a g) | fis( g) r4 r2 |
  
				%page 26
  R1-\markup "Solo" |
  e'2( c4 ) r| R1*4 |
  %%
  R1*4 |
  %%
  R1*4 |
				%page 27
  R1*4 | r2 r4 r8 ees\p |
  %%
  c a fis ees' c aes g fis | g4 r r2 | R1 |
  r8 c b d c e? f d | e4 r r2 |

				%page 28
  R1 | g2( e4 c) | g r r2 | R1 |
  %%
  b1 | c1\f^\markup "Tutti"~ | c4 a b cis |
  d1~ | d4 b c? d | ees1~ |
  
				%page 29
  ees1~ | ees1~ | ees1~ | ees1~ | ees4( e f fis |
  g) r b, r | c r fis r | g2 r2\fermata |
  c,2\f~( c8 des c b) |
  %%
  c2~( c8 des c b) | c2~( c8 des c b) |
  c1~ | c2. d?4 | ees2. fis4 | g2( e?4 c) |
  f?2( d4 b) |

				% page 30 
  c8 r cis,4( d e | f fis g gis) | a r r2 | R1 |
  c8\f r g r c r e r | g1 |
  f4. d16( b) c4. a16 (fis) |
  %%
  g4 e' r d | c r fis8( g) g-. g-. | 
  g4 r dis8( e) e-. e-. | 
  e4 r b8( c) c-. c-. |
  c8 r c r c r c r |
  c4 r r2 \bar "|."
}