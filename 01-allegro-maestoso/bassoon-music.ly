\version "2.14.0"

\parallelMusic #'(bassoonFirst bassoonSecond) {
  %%1
  R1*6 |
  R1*6 |
  %%7
  c4\p c8. c16 d4 d8. d16 |
  g4\p g8. g16 b4 b8. b16 |
  %8
  e4 b8-. c-. g4 r |
  c4 b,8-. c-. g4 r |

  R1*2 |
  R1*2 |


  c4\p c8. c16 d8.\trill c16 d8. e16 |
  g'4 g8. g16 b8.\trill a16 b8. g16 |

  c8\f r g, r c r e r |
  e8\f r g, r c r e r |

  f4. (\times 2/3 {e16 d c} b4) r8. g16 |
  f4. (\times 2/3 {e16 d c} b4) r8. g16 |

  b8 r d r f r d r |
  b8 r d r f r d r |
  
  %%bar 15
  g4.( \times 2/3 {a16 g f} e4) r8. g16 |
  g4.( \times 2/3 {a16 g f} e4) r8. g16 |

  c,8 r e r a r c r |
  c,8 r e r a r c r |

  gis4.( \times 2/3 {a16 gis fis} e4) r8. gis16 |
  gis4.( \times 2/3 {a16 gis fis} e4) r8. gis16 |

  a8 r e r c' r a r |
  a8 r e r c' r a r |

  fis4.( \times 2/3 {g16 fis e} d4) r |
  fis4.( \times 2/3 {g16 fis e} d4) r |

  g'1 |
  g1 |

  %% 21
  fis1 |
  a1   |

  %% bar 22
  f!1 |
  b1  |
  
  e |
  c |
  
  f4.( e8) f4.( e8)     |
  d4. (cis8) d4. (cis8) |
  
  f8( e) f8( e) f8( e) f8( e)         |
  d8( cis) d8( cis) d8( cis) d8( cis) |
  
  f4 r r2 |
  d4 r r2 |

  f8\p-. e-. d-. c-. b-. a-. g-. f-.  |
  d8\p-. c!-. b-. a-. g-. f-. e-. d-. |

  e4 r r2 |
  c4 r r2 |

  r1 |
  r1 | 

  %%bar 30
  d'2.( e4) |
  b'2.( c4) |

  f4( d b d) |
  d4( b g b) |

  e4 r r2 |
  c4 r r2 |

  r1 |
  r1 |

  d2.( e4) |
  b2.( c4) |

  f4( d b d) |
  d4( b g b) |

  c4 r r2 |
  c4 r r2 |

  R1*3 |
  R1*3 |
  
  %%bar 40
  c8\f r g r c r e r |
  c8\f r g r c r e r |
  
  f4.( \times 2/3 {e16 d c} b4) r |
  f4.( \times 2/3 {e16 d c} b4) r |

  g'1~ |
  g1~  |

  g1  |
  g1~ |

  c,2~( c8 des c b) |
  g2 aes            |

  %%bar 45
  c2~( c8 des c b) |
  g2 aes            |

  c2~( c8 des c b)  |
  g2 aes            |

  c8 e, e e e e e e |
  g8 e e e e e e e  |

  f8 f f f f f f f |
  f8 f f f f f f f |

  fis8 fis fis fis a a a a |
  fis8 fis fis fis a a a a |

  g8 g g g g g g g |
  g8 g g g g g g g |

  g8 g g g g g g g |
  g8 g g g g g g g |

  %%bar 52
  c,4 a'8\p  a  a a c c |
  c,4 a8\p a d d c c    |

  c8 c d d d d e e |
  f8 f d d g g e e |

  f4 r r2 |
  f4 r r2 |

  r1 |
  r1 |

  g,4 a8 a a a c c  |
  c8 c a a d d c c  |

  c8 c d d d d e e |
  f8 f d d g g e e |

  f4 r r2 |
  f4 r r2 |
  
  R1*6 |
  R1*6 |

  %%bar 65
  c8\f r g r c r e r  |
  c'8\f r g r c r e r |
  
  f4. d16( b) c4. (a16 fis) |
  f4. d16( b) c4. (a16 fis) |

  g4 r g, r |
  g4 r g, r |

  c4 r r2 |
  c4 r r2 |
  
  r1 |
  r1 |

  %%bar 70
  %% non si capisce se battuta 70 e' per solo o a due
  %% le successive 71 e 72 sono a solo
  %% ascoltare il pezzo
  r4 r8 g'8\p fis( g a g ) |
  r4 r8 g'8\p fis( g a g ) |
  
  c8( \times 2/3 { d16 c b} c8) [d-.] e4( fis) |
  r1                                         |
  
  g4 r r2 |
  r1      |

  R1*3 |
  R1*3 |

  f1\p~ |
  b1\p~ |

  f1~ |
  b1~ |

  f1~ |
  b1~ |

  f4 r r2\fermata |
  b4 r r2\fermata |

  R1*4 |
  R1*4 |

				% page 7
  R1*6 |
  R1*6 |

  c4\p c8.  c16 d8.\trill c16 d8. e16 |
  g4\p g8. g16 b8. a16 b8. g16 |

  c4 r r2 |
  e4 r r2 |
  
  R1*5 |
  R1*5 |

				%page 8
  R1*10 |
  R1*10 |
  
  g2\f r |
  g,2\f r |

  g,4. g16( a b8) b16( c d8) \times 2/3 {d16( e fis} |
  g4. g16( a b8) b16( c d8) \times 2/3 {d16( e fis} |

  g4) r r2 |
  g4) r r2 |
  
  R1*3 |
  R1*3 |
  
				%page 9
  
  R1*23 |
  R1*23 |
  
  r8 b(\p c d) r b( c d) |
  r8 g(\p a b) r g(a b) |

				% page 10
  r8 (c d e ) r  c( d e) |
  r8 a( b c ) r a (b c ) |
  
  r8 a,( b c ) r a (b c ) |
  r8 fis,( g a) r fis( g a) |

  r8 b( c d) r b( c d) |
  r8 g( a b) r g(a b) |

  R1*3 |
  R1*3 |

  R1*4 |
  R1*4 |

  g,8\p r d r g r b r |
  g,8\p r d r g r b r |

				% page 11 
  R1*14 |
  R1*14 |
  
				%page 12
  r4 dis\f e d |
  r4 dis\f e d |
  
  
  c4 r r2 |
  c4 r r2|

  R1*17 |
  R1*17 |

				%page 13
  R1*4 |
  R1*4 |
  
  R1*2 |
  R1*2 |

  r8 g'\p e dis bes g e cis |
  r4 r8 dis\p bes g e cis |
  
  d4 r r2 |
  d4 r r2 |

  r1 |
  r1|

  d'2 b!4 g |
  r1        |

  d'2 b4 g |
  d'2 b!4 g |

  d4 r r2 |
  d4 r r2 |

				%page 14
  r1 |
  r1 |

  g8\f r d r g r b r |
  g'8\f r d r g r b r |

  c4.( \times 2/3 {b16 a g} fis4) r8. d16 |
  c4.( \times 2/3 {b16 a g} fis4) r8. d16 |

  fis8 r a r c r a r |
  fis8 r a r c r a r |

  d4.( \times 2/3 {e16 d c} b4) r8. d,16 |
  d4.( \times 2/3 {e16 d c} b4) r8. d,16 |

  g8 r bes r d r bes r |
  g8 r bes r d r bes r |

  ees1 |
  g1 |

  f |
  aes |
  
  ees2 f |
  g2 aes |

  ees4( f ees f) |
  g4( aes g aes) |

  ees8 ees, ees ees  ees ees ees ees |
  g8 ees ees ees  ees ees ees ees |

  d8 d d d d d d d |
  d8 d d d d d d d |
  
  g4 b8\p b a a d d |
  g,4 e'8\p e a, a g g |
 
  c c e e d d fis fis |
  c c a a d d b b |

  e4 r r2 |
  c4 r r2 |

				%page 15 
  r1 |
  r1 |

  b8 b b b a a d d |
  g'8 g e e a, a g g |

  c8 c e e d d fis fis |
  c8 c a a d d b b |

  e4 r r2 |
  c4 r r2 |

  R1*4 |
  R1*4 |

  R1*8 |
  R1*8 |

  R1*7 |
  R1*7 |

				%page 16 
  r1 |
  r1 |

  b2(\p g4) r |
  g'2(\p e4) r |

  r4 c,( fis a) |
  r4 a,( c fis) |

  c2( a4) r |
  a2( fis4) r |

  b2( a |
  g2( fis |

  g2 fis) |
  e2 dis) |

  e4 r f'!2( |
  e4 r r2 |

  e4) r f2( |
  r1|

  e4) r r2 |
  r1 |

  r1 |
  r1 |
  
				%page 17 

  r2 cis8( d e cis) |
  r2 bes'2( |
  
  e4 r cis8( d e cis) |
  a4) r bes2( |

  e4 r r2 |
  a4) r r2 |

  r1 |
  r1 |

  r2 ees( |
  r1 |

  d4) r ees2( |
  r1 |

  d4) r r2 |
  r1 |

  r1 |
  r1 |

  r2 b!8( c d b) |
  r2 a( |

  f'4 r b,8( c d b) |
  g4) r g2 |

				%page 18 |

  c4 r e!8( f g e) |
  c,4 r r2 |

  g4 r e8( f g e) |
  r1 |
  
  f4 r r2 |
  r1 |

  r1|
  r1|

  R1*9 |
  R1*9 |

  R1*4 |
  R1*4 |
  
				%page 19 
  d1( |
  b'1( |

  ees1) |
  c1) |

  d2( c |
  b2( a |
  
  b2 a) |
  g2 f!) |

  g2( f |
  e!2( d|

  e2 d) |
  c2 b) |

				% page 20 

  c4 r r2 |
  c4 r r2 |

  R1*5 |
  R1*5 |

  c'4\p c8. c16 d4 d8. d16 |
  g'4\p g8. g16 b4 b8. b16 |

  e4 b8-. c-. g4 r |
  c4 b,8-. c-. g4 r |

  R1*2 |
  R1*2 |

  c4 c8. c16 d8.\trill c16 d8. e16 |
  g'4 g8. g16 b8.\trill a16 b8. g16 |

  c8\f r g, r c r e r |
  e8\f r g, r c r e r |

  f4.( \times 2/3 {e16 d c } b4) r8. g16 | 
  f4.( \times 2/3 {e16 d c } b4) r8. g16 | 

				%page 21 
  b8 r e r f r d r |
  b8 r e r f r d r |

  g4. ( \times 2/3 {a16 g f } e4) r8. g16 |
  g4. ( \times 2/3 {a16 g f } e4) r8. g16 |

  c,8 r e r a r c r | 
  c,8 r e r a r c r | 

  gis4.( \times 2/3 {a16 gis fis} e4) r8. g16 |
  gis4.( \times 2/3 {a16 gis fis} e4) r8. g16 |

  a8 r e r c' r a r |
  a8 r e r c' r a r |

  fis 4.( \times 2/3 {g16 fis e } d4) r8. fis16 |
  fis 4.( \times 2/3 {g16 fis e } d4) r8. fis16 |


  g8 r d r bes' r g r |
  g8 r d r bes' r g r |

  e4.( \times 2/3 {f!16 e d} c4) r8. e16 |
  e4.( \times 2/3 {f!16 e d} c4) r8. e16 |

  f4 r r2 |
  f4 r r2 |

  R1*3 |
  R1*3 |
  
				%page 22 
  R1*6 |
  R1*6 |

  R1*4 |
  R1*4 |

  d'1\p~ |
  b1\p~ |

  d1~|
  b1~ |

  d1~  |
  b1~ |

  d2( f) |
  b2( d) |

  e4 r r2 |
  c4 r r2 |

  r1 |
  r1 |

				%page 23

  R1*6 |
  R1*6 |

  r8 c(\p d e) r c( d e) |
  r4 c,\p r c |

  r8 d( e f ) r d( e f) |
  r4 f r f |

  r8 b,( c d ) r b( c d) |
  r4 g r g |

  r8 c( d e) r c( d e ) |
  r4 e r e |
  
  R1*7 |
  R1*7 |

  c8\p r g r c r e r |
  c8\p r g r c r e r |
  
				%page 24 
  r1 |
  c'4.( \times 2/3 {b16 a g } f4) r |

  f4.( \times 2/3 {e16 d c} b4) r |
  r1 |
  
  r1 |
  b4. ( \times 2/3 {a16 g f} e4) r |
  
  e4.( \times 2/3 {d16 c b} a4) r |
  r1 |

  r1 |
  a4.( \times 2/3 {g16 f e} d4) r |

  d4.( \times 2/3 { c16 b a} g4) r |
  r1 |

  R1*2 |
  R1*2 |

  R1*4 |
  R1*4 |

				%page 25
  R1*5 |
  R1*5 |

  f'!8-.\p e-. d-. c-. b-. a-. g-. f-. |
  d'8-.\p c-. b-. a-. g-. f-. e-. d-. |

  e4 r r2 |
  c4 r r2|

  r2 r4 r8 g |
  r1 |

  g8( f' e d c b a g) |
  r2 r4 r8 g'( |

  fis8 g r g' g( fis f d) |
  fis8 g a g) g( fis f d) |
  
				%page 26
  c4 r r2 |
  c4 r r2 |

  e2( c4)  r |
  g'2( e4) r |

  d2.( e4 |
  b'2.( c4 |

  f4 d b d) |
  d4 b g b) |

  e4 r r2 |
  c4 r r2 |

  R1*9 |
  R1*9 |
  
				%page 27 
  d1\p |
  d,2\p( g) |

  
  c4 r r2 |
  c,4 r r2|
  
  R1*2 |
  R1*2 |

  r1 |
  r2 r4 r8 ees' |

  r4 r8 ees c aes g fis |
  c a fis ees c aes g fis |

  g8 c b d c e! f d |
  g8 e'! d f! e g a b |

  e4 r r2 |
  c4 r r2 |

  R1*2 |
  R1*2 |

				%page 28 
  r1 |
  r1 |

  g2( e4 c) |
  r1 |

  g4 r r2 |
  g,2( e4 c) |

  r1|
  g'4 r r2 |

  r1|
  r1|

  c,8\f r g r c r e r |
  c8\f r g r c r e r |

  f4.( \times 2/3 {e16 d  c} b4) r8. g16 |
  f4.( \times 2/3 {e16 d  c} b4) r8. g16 |

  b8 r d r f r d r |
  b8 r d r f r d r |
  
  g4.( \times 2/3 {a16 g f} e4) r8. g16 |
  g4.( \times 2/3 {a16 g f} e4) r8. g16 |
  
  c,8 r ees r g r c r |
  c,8 r ees r g r c r |

  
				%page 29
  c1( |
  aes1 ( |

  des1) |
  bes1) |

  c2( des) |
  aes2( bes) |

  c4 ( des c des) |
  aes4 ( bes aes bes|

  c1 |
  aes4) e! f fis |

  b!4 r f r |
  g4 r f r |

  e4 r a! r |
  e4 r a! r |

  g2 r\fermata | 
  g2 r\fermata | 

  c2~(\f c8 des c b) |
  e4\f( g) aes2 |

  c2~(\f c8 des c b) |
  g2 aes2 |

  c2~(\f c8 des c b) |
  g2 aes2 |

  c8 e, e e e e e e |
  g8 e e e e e e e |

  f8 f f f f f f f |
  f8 f f f f f f f |

  fis8 fis fis fis a a a a |
  fis8 fis fis fis a a a a |

  g8 g g g g g g g |
  g8 g g g g g g g |

  g8 g g g g g g g |
  g8 g g g g g g g |

				%page 30
  c8 c\p a a a a c c |
  c,8 r a a d d c c |

  c c d d d d e e |
  f f d d g g e e |

  f4 r r2 |
  f4 r r 2 |

  R1*2 |
  R1*2 |

  c8\f r g r c r e r |
  c'8\f r g r c r e r |

  f4. d16( b) c4. (a16 fis) |
  f4. d16( b) c4. (a16 fis) |
  
  g4 r g, r |
  g4 r g, r |

  c4 r fis'8(\p g) g-. g-. |
  c4 r dis'8(\p e) e-. e-. |

  g4 r dis8( e) e-. e-. |
  e4 r b8( c) c-. c-. |
  
  e4 r b8( c) c-. c-. |
  c4 r dis,8( e) e-.  e-. |

  c8 r c r c r c r |
  e r e r e r e r |

  c4 r r2 |
  e4 r r2 \bar "|."
}