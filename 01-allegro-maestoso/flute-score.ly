\version "2.14.0"
\include "global.ly"
\include "flute-music.ly"

\header {
  instrument = "Flute"
}

\score {
  \new Staff {\global \fluteMusic}
}