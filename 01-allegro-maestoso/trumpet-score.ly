\version "2.14.0"
\include "global.ly"
\include "trumpet-music.ly"

\header {
  instrument = "Trumpet"
}

\score {
  \new Staff {
%%%    \partcombine {\global \trumpetFirst} {\global \trumpetSecond}
    \partcombine {\relative c'' {\clef treble \global  \trumpetFirst}} {\relative c' {\clef treble \global \trumpetSecond}}
  }
}