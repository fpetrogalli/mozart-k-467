\version "2.14.0"
\include "global.ly"

\include "timpani-music.ly"
\header {
  instrument = "Timpani"
}

\score {
  \new Staff {\global \Timpani}
}