\parallelMusic #'(oboeFirst oboeSecond) {
  R1*6 |
  R1*6 |

  c4\p c8. c16 d4 d8. d16 |
  g4\p g8. g16 b4 b8. b16 |
				%page 2
  e8-\trill (d16 e) f8-. e-. d4 r |
  c8-\trill b16 c  d8-. c-. b4 r |

  R1*2 |
  R1*2 |

  c4 c8. c16 d8.-\trill c16 d8. e16 |
  g4 g8. g16 b8.-\trill a16 b8. g16 |

  c2 e |
  e2\f c'~ |

  d2. e4 |
  c4 a b cis |

  f1 |
  d1~ |


  %%bar 15
  e2 g4 b |
  d4 b c! d |

  c1 |
  e1~ |

  b2 gis4 b |
  e4 dis e d! |

  e, a2 c4 |
  c2 ees~ |

  a2 fis4 a |
  ees4 cis d c! |

  g1 |
  b2 g |

  fis1 |
  a1 |

  %%bar 22
  f!1 |
  b1  |
  
  e1 |
  c1 |

  f4.( e8) f4.( e8)     |
  d4. cis8 d4. cis8 |
  
  f8( e8) f8( e8) f8( e8) f8( e8)         |
  d8 cis8 d8 cis8 d8 cis8 d8 cis8 |
  
  f4 r r2 |
  d4 r r2 |
  
  d'8\p-. c-. b-. a-. g-. f-. e-. d-. |
  r1                                 |

  c4 r r2 |
  r1      |
  
  e8-.\p \grace f32^( e16 d e8) f8-. g4-. r4 |
  c8-.\p \grace d32^( c16 b c8) d8-. e4-. r4 |

  %% bar 30
  r1 |
  r1 |

  fis8( g a g) g( fis f d) |
  r1                       |

  c4 r r2 |
  r1      |

  e8-.\p \grace f32^( e16 d e8) f8-. g4-. r4 |
  c8-.\p \grace d32^( c16 b c8) d8-. e4-. r4 |
  
  r1 |
  r1 |

  fis8( g a g) g( fis f d) |
  r1                       |

  c4 r r2 |
  r1      |

  R1*3    |
  R1*3    |
  
  %%bar 40
  c8\f r g r c r e r | 
  c8\f r g r c r e r | 
  
  f4.( \times 2/3 {e16 d c} b4) r |
  f4.( \times 2/3 {e16 d c} b4) r |

  g'1~ |
  g1 ~ |
  
  g1~ |
  g1~ |
  
  g2  aes |
  g2  aes | 
  
  %%bar 45
  g2  aes |
  g2  aes | 
  
  g2  aes |
  g2  aes | 

  g4 g2 gis4~( |
  g4 g2 gis4~( |
  
  gis8 a!) a2 b4~( |
  gis8 a!) a2 b4~( |

  b8 c) c2 fis,4 |
  b8 c) c2 fis,4 |

  g4 e8. e16 e4 e  |
  g4 c8. c16 c4 c |

  d4 d8. d16 d4 d |
  b4 b8. b16 b4 b |

  %% bar 52
  c4 g8\p g f f bes bes |
  c4 e,8\p e d d g g    |

  a a c c b! b d d |
  f f a a g g b b  |
  
  c4 r r2 |
  a4 r r2 |

  r1 |
  r1 |

  g8 g g g f f bes bes |
  e8 e e e d d g g    |

  a a c c b! b d d |
  f f a a g g b b  |
  
  c4 r r2 |
  a4 r r2 |

  R1*3 |
  R1*3 |
  
  %bar 62
  d2( ees) |
  c1~      |

  e!2( f) |
  c2( d)  |
  
  e8\f r g, r c r e r |
  c8\f r g r c r e r  | 
  
  g4 g8. g16 g4 g |
  g4 g8. g16 g4 g |

  f4. d'16( b) c4. a16( fis) |
  f4. d16( b) c4. a16( fis)  |

  g4 c r d |
  g e' r d |
  
  c r8 g\p fis( g a g |
  e4 r r2             |
  
  c4) e,2( fis4) |
  r1             |

  %%bar 70
  g4 r r2 |
  r1      |

  R1*5 |
  R1*5 |

  d1\p~ |
  b1\p~ |

  d1~ |
  b1~ |

  d1~ |
  b1~ |

  d4 r r2\fermata |
  b4 r r2\fermata |
  
  R1*4 |
  R1*4 |

				%PAGE 7
  R1*6 |
  R1*6 |
  %%
  c4\p c8. c16 d8.\trill c16 d8. e16 |
  g4\p g8. g16 b8.\trill a16 b8. g16 |

  c4 r r2 |
  e4 r r2 |

  R1*5 |
  R1*5 |
  
				%page 8
  R1*5 |
  R1*5 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1 |
  R1 |

  g2\f r |
  g2\f r |

  g'2 g4 g4 |
  b2 b4 b4 |

  g4 r r2 |
  b4 r r2 |
  
  R1*3 |
  R1*3 |

				%page 9
  R1*9 |
  R1*9 |
  %%
  R1*4|
  R1*4|
  %%
  R1*5|
  R1*5|
  %%
  R1*5|
  R1*5|

  d8(\p b a g ) d' (b a g ) |
  R1 |

				%page 10
  e'8( c b a) a2 |
  R1 |

  c8 ( a g fis) c' ( a f fis) |
  R1 |

  d'8( b a g) g2 |
  R1 |

  r4 d'4( ees d) |
  R1 |

  R1*2 |
  R1*2 |
  %%
  R1*4 |
  R1*4 |
  
  g8\p r d r g r b r |
  g8\p r d r g r b r |

				%page 11
  R1 |
  R1 |

  c1 ~|
  a'1~ |

  c1 |
  a1 |

  b1~ |
  g1~ |

  b1 |
  g1 |
  %%
  a1 |
  fis1 |

  g4 r r2 |
  g4 r r2 |
  
  R1*2 |
  R1*2 |
  %%
  R1*5 |
  R1*5 |
  
				%page 12
  r4 c,4\f b b |
  r4 a, g f! |

  c4 r r2 |
  e4 r r2 |

  R1*4 |
  R1*4 |
  %%
  R1*5 |
  R1*5 |
  %%
  R1*4 |
  R1*4 |
  %%
  R1*4 |
  R1*4 |

				%page 13  
  R1*4 |
  R1*4 |
  %%
  R1 |
  R1 |
  
  r2 r4 r8 cis'8\p |
  r2 r4 r8 cis'8\p |

  bes8 g e cis  bes g e cis |
  bes8 g e r r2 |

  d4 r r2 |
  R1 |
  %%
  R1 |
  R1 |
  
  g'1 ~ |
  d'2 b!4 g |

  g1~ |
  d4 r r2 |
  
  g4 r r2 |
  R1 |
 
				%page 14
  R1 |
  R1 |
  
  g1~\f |
  b'1\f |

  g4 e fis gis |
  a2. b4 |

  a1~ |
  c1 |

  a4 fis g! a |
  b2 d4 fis |

  bes1 |
  g1 |

  bes,1~ |
  g,1 |
  %%
  bes1~ |
  aes1 |

  bes1 |
  g2 aes2 |

  bes1~ |
  g4( aes g aes ) |
  
  bes4 b c cis |
  g2 g |
  
  d4 r a r |
  b4 r fis r |

  g4 (gis\p a b |
  g4 d8\p d c c f! f |

  c4 cis d dis)|
  e8 e g g fis fis a a |
  
  e4 r r2 |
  g4 r r2 |
  
				%page 15
  R1 |
  R1 |

  g,4 (gis a b |
  g4 d8 d c c f! f |

  c4 cis d dis)|
  e8 e g g fis fis a a |
  
  e4 r r2 |
  g4 r r2 |
  
  R1*4 |
  R1*4 |
  %%
  R1*3 |
  R1*3 |

  fis2\p( g) |
  dis'\p( e) |

  fis2( g) |
  dis (e) |
  
  fis4 r fis r |
  dis4 r dis r|

  fis4 r  r2 |
  dis4 r  r2 |

  R1 |
  R1 |
  %%
  R1*7 |
  R1*7 |

				%page 16
  g2\p( e4) r |
  r1 |

  r4 b( e g) |
  r1 |

  a2( fis4) r |
  r1 |

  r4 c( fis a) |
  r1 |

  g2( fis |
  r1 |
  %%
  e2 dis) |
  r1 |

  d!4 r gis,8( a  b gis) |
  r1 |

  d'4 r  gis,8( a b gis) |
  r1 |

  d'4 r r2 |
  r1 |

  r1|
  r1|

				%page 17
  
  r2 g!8( f e g ) |
  r1 |

  cis,4 r g'8( f e g) |
  r1|

  cis,4 r r2 |
  r1 |

  r1 |
  r1 |

  r2 fis,8( g a fis) |
  r1 |
  %%

  c'4 r fis,8( g a fis) |
  r1 |
  
  c'4  r r2|
  r1 |

  r1 |
  r1 |

  r2 f!8( ees d f) |
  r1 |

  b,4 r f'8( ees d f) |
  r1 |

				%page 18 
  e!4 r g8( f e g) |
  r2 g,8( aes bes g) |

  c,4 r g'8( f e g) |
  bes4 r g8 (aes bes g) |

  f4 r r2 |
  aes4 r r2 |

  r1 |
  r1 | 
  %%
  r4 g,( aes bes) |
  r1 |

  c1~ |
  r1 |
  
  c4 f,( g a!) |
  r1 |

  b!4 r r2 |
  r1 |

  R1*5 |
  R1*5 |

				%page 19 
  R1*4 |
  R1*4 |

  d1( |
  b1( |

  ees1) |
  c1) |

  d2 a'( |
  b4 r r2 |

  g2 f!) |
  r1 |

  g2 ( f |
  e!2 (d |

  e d) |
  c b) |
  
				%page 20
  
  c4 r r2 |
  c4 r r2 |
  
  R1*5 |
  R1*5 |

  c4\p c8. c16 d4 d8. d16 |
  g4\p g8. g16 b4 b8. b16 |
  %%
  e8\trill( d16 e) f8-. e-. d4 r |
  c8\trill( b16 c) d8-. c-. b4 r |

  R1*2 |
  R1*2 |

  c4 c8. c16 d8.\trill c16 d8. e16 |
  g4 g8. g16 b8.\trill a16 b8. g16 |

  c2\f e |
  e2\f c'~ |

  d2. e4 |
  c4 a b cis |

				%page 21
  f1 |
  d1~|

  e2 g4 b |
  d4 b c! d |

  c1 |
  e1~|
  
  b2 gis4 b |
  e4 dis e d |

  e,4 a2 c4 |
  c2 ees~ |

  a2 fis4 a |
  ees4 cis d c |
  %%
  d,4 g2 bes4 |
  bes2 des2~ |
  
  g2 e4 g |
  des4 b! c bes |
  
  f!4 r r2 | 
  a4 r r2 |

  R1*3 |
  R1*3 |

				% page 22
  r1 |
  r1 |
  
  bes,8\p r g r bes r des r |
  g1\p |

  c2( bes |
  aes2( g |

  c bes) |
  aes g) |

  c4 r r2 |
  aes4 r r2 |

  r1 |
  r1 |

  R1*4 |
  R1*4 |

  r1 |
  f1\p~ |
  %%
  r1 |
  f1~ |

  b1~ |
  f1~ |

  b2( d4 f ) |
  f2( b4 d) |

  e4 r r2 |
  c4 r r2 |

  r1 |
  r1 |
  
				% page 23

  R1*6 |
  R1*6 |

  g8( e d c) g'( e d c) |
  r8 e,( f g ) r e( f g) |
  
  a'8( f e d) d2 |
  r8 ( f( g a) r f( g a) |
  
  f8( d c b) f'( d c b) |
  r8 d,( e g) r d( e g) |

  g'8( e d c) c2 |
  r8 ( e f g) r e( f g) |
  
  r4 g( aes g) |
  r1 |

  R1*2 |
  R1*2 |
  %%
  R1*4 |
  R1*4 |

  c8\p r g r c r e r |
  r1 |
  
				%page 24
  
  a1~ |
  r1 |

  a1 |
  d'1~ |

  g1~ |
  d1 |

  g1 |
  c1~ |
  %%
  f1~ |
  c1 |

  f1 |
  b1 |

  e4 r r2 |
  c4 r r2 |

  r1 |
  r1 |
  
  R1*4 |
  R1*4 |
  
				%page 25
  R1*4 |
  R1*4 |
  %%
  r1 |
  r1 |

  f!8-.\p e-. d-. c-. b-. a-. g-. f-. |
  d-.\p c-. b-. a-. g-. f-. e-. d-. |
  
  e4 r r2 |
  c4 r r2 |

  e'8-. \grace f32^ ( e16 d e8) f8-. g4-. r4 |
  c'8-. \grace d32^ ( c16 b c8) d8-. e4-. r4 |
    
}
