\version "2.14.0"

\include "global.ly"
\include "oboe-music.ly"

\header {
  instrument = "Oboi"
}


\score {
  \new Staff {
%    \partcombine {\global \oboeFirst} {\global \oboeSecond}
    \partcombine {\relative c'' {\clef treble \global  \oboeFirst}} {\relative c'' {\clef treble \global \oboeSecond}}
  }
}