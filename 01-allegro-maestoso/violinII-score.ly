\version "2.14.0"

\include "global.ly"
\include "violinII-music.ly"
\header {
  instrument = "Violin II"
}

\score {
  \new Staff {
    \global \violinII
  }
}