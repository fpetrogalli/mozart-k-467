\version "2.14.0"

Timpani= \relative c {
%  \key c \major
%  \time 4/4
  \clef bass

				%page 1
  R1*6 |
  c4\p c8. c16 g4 g8. g16 |
				%page 2
  c4 g8 c g4 r |
  R1*2 | 
  c4 c8. c16 g4 g4 |
  c4\f r4 r2 |
  g2 g4 g |
  g r r2 |
  
  g2 g4 g |
  c r r2 |
  R1*3 |
  g4 r r2 |
  g4 r r2 |

  %%bar 22
  g4 r r2 |
  g4 r r2 |
  g4 r g r |
  g g g g |
  g r r2 |
  R1*15 |

  %bar 42
  g4\f r r2 |
  g4 r r2 |
  c4 r r2 |
  
  %%bar 45
  c4 r r2 |
  c4 r r2 |
  c4 r r2 |
  c4 r r2 |
  c4 c8. c16 c4 c |
  g4 r r2 |
  g2 g4 g |

  %%bar 52
  c4 r r2 |
  R1*12 |
  
  %%bar 65
  c8\f r g r c r e r |
  g,4 g8. g16 c4 c8. c16 |
  g4 r g r |
  c4 r r2 |
  R1*10 |
  r1\fermata|

  R1*4|
  
				%page 7
  R1*6 |
  %%
  c4\p c8. c16 g4 g | c r r2 |
  R1*5 |

				%page 8
  R1*5|
  %%
  R1*4|
  %%
  R1  | g4\f r r2 | g2 g4 g | g r r2 | R1*3 |

				%page 9
  R1*9 |
  %%
  R1*4 |
  %%
  R1*5 |
  %%
  R1*6 |

				%page 10
  R1*6 |
  %%
  R1*5 |

				%page 11
  R1*5 |
  %%
  R1*4 |
  %%
  R1*5 |

				%page 12
  R1*6 |
  %%
  R1*5 |
  %%
  R1*4 |
  %%
  R1*4 |
  
				%page 13
  R1*4 |
  %%
  R1*4 |
  %%
  R1*4 |

				%page 14
  R1 |
  g4\f r r2 |
  R1*3 |
  g4 g8. g16 g4 g |
  g r r2 |
  %%
  R1*3 |
  g4 r  g r |
  R1 |
  g4 r r2 |
  R1*2 |

				%page 15
  R1*8 |
  %%
  R1*8 |
  %%
  R1*7 |

				%page 16
  R1*5 |
  %%
  R1*5 |
				%page 17
  R1*5 |
  %%
  R1*5 |

				%page 18
  R1*4 |
  %%
  R1*4 |
  %%
  R1*5 |
  
				%page 19 
  R1*5 |
  %%
  R1 |
  g4\p r r2 |
  g4 r r2 |
  g4 r r2 |
  g4 r r2 |
  
				%page 20
  c4 r r2 |
  R1*6 |
  %%
  c4 g8-. c-. g4 r |
  c4 r r2 |
  g4 r r2 |
  c4 c8. c16 g4 g |
  c4\f r r2 |
  g2 g4 g |

				%page 21
  g4 r r2 |
  g2 c4 g |
  c r r2 |
  R1*3 |
  %%
  g4 r r2 |
  c2 c4 c |
  c r r2 |
  R1*3 |
  
				%page 22
  R1*6 |
  %%
  R1*5 |
  %%
  R1*5 |

				%page 23
  R1*6 |
  %%
  R1*7 |
  %%
  R1*5 |
  
				%page 24
  R1*4 |
  %%
  R1*4 |
  %%
  R1*4 |

				%page 25
  R1*4 |
  %%
  R1*6 |

				%page 26
  R1*6 |
  %%
  R1*4 |
  %%
  R1*4 |

				%page 27 
  R1*5 |
  %%
  R1 | 
  g4\p r r2 |
  g4 r r2 |
  g4 r r2 |
  R1 |

				%page 28
  R1*4 |
  %%
  R1 | c4\f r r2 |
  g2 g4 g |
  g r r2 |
  g2 c4 g |
  c c8. c16 c4 c |
  
				%page 29
  c4 r r2 |
  R1*3 |
  c4 c8. c16 c4 c |
  g4 r g r |
  c r c r |
  g2 r2\fermata |
  c4\f r r2 |
  %%
  c4 r r2 |
  c4 r r2 |
  c2 c4 c |
  c4 r r2 |
  c4 c8. c16 c4 c |
  g4 r r2 |
  g2 g4 g |

				%page 30
  c4 r r2 |
  R1*4 |
  c8\f r g r c r c r | 
  g4 g8. g16 c4 c8. c16 |
  %%
  g4 g8. g16 g4 g |
  c4 r r2 |
  c4\p r r2 |
  c4 r r2 |
  c8 r r4  c8 r r4 |
  c4 r r2 \bar "|."
}