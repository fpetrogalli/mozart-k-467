\version "2.14.0"

cdb=\relative c {
  \clef bass
  				%page 1
  \pageCounterOriginalEditions
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r |
  r8 c' c c c4 r |
  r8 g g g g4 r |
  R1 |
				%page 2
  \pageCounterOriginalEditions
  R1 |
  r8 c c c c4 r |
  r8 g g g g4 r |
  R1 |
  c,8\f r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 
  b8 r d r f r d r |

  g4.( \times 2/3 {a16 g f} e4) r8. g16 |
  c,8 r e r a r c r |
  gis4.( \times 2/3 {a16 gis fis} e4) r8. gis16 |
  a8 r e r c' r a r |
  fis4.( \times 2/3 {g16 fis e} d4) r8. fis16 |
  g8 g g g g g g g |
  g g g g g g g g |

				%page 3
  \pageCounterOriginalEditions
  g8 g g g g g g g |
  g8 g g g g g g g |
  g8 g g g g g g g |
  g8 g g g g g g g |
  g4 r r2 |
  R1 |
  c,4\p c c c |
  c c c c|

  g'1 |
  g, |
  c4 c c  c |
  c c c c |
  g'1 |
  g, |
  c4 r r2 |
  R1 |

				%page 4
  \pageCounterOriginalEditions
  R1 |
  d'8 r g, r d' r f r |
  e4.(\f \times 2/3 {f16 e d} c4) r |
  d,8 r g, r d' r f r |
  e4.( \times 2/3 {f16 e d} c4) r |
  b'4.( \times 2/3 {c16 d e} f4) f, |
  e 8 e e e f f f f|

  e 8 e e e f f f f|
  e 8 e e e f f f f|
  e e e e e e e e |
  f f f f f f f f |
  fis fis fis fis a a a a |
  g g g g g g g g |
  g g g g g g g g |
  
				%page 5
  \pageCounterOriginalEditions
  c,4 r r2 |
  R1 |
  f8\p f f f f f f f |
  g g g g g, g g g |
  c4 r r2 |
  R1
  f8 f f f fis fis fis fis |
  g g g g gis gis gis gis |
  a a a a aes aes aes aes |

  g g g g fis fis fis fis |
  f! f f f fis fis fis fis |
  g g g g g, g g g |
  c4 r r2 |
  c'8\f r g r c r e r |
  f4. d16(b) c4. a16(fis) |
  g4 r g, r |
  c r r2 |
  R1 |

				%page 6
  \pageCounterOriginalEditions
  R1 |
  a1\p( |
  g4) r r2 |
  aes'1( |
  g4) r r8 g g g |
  g4 r r8 g g g |
  g4 r r2 |

  R1*2 |
  r1\fermata 
|
  c,8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r |

				%page 7
  \pageCounterOriginalEditions
  R1*4 |
  c'4 r r2 |
  g4 r r2 |

  R1*4 |
  c,1~ |
  c~ |
  c |
  
				%page 8
  \pageCounterOriginalEditions
  c4 r r2 |
  R1*7 |
  g'1~ |

  g |
  g,4\f r r2 |
    g4. g16(a b8) b16(c d8) \times 2/3 {d16( e fis} |
  g4) r r2 |
  R1*3 |

				%page 9
  \pageCounterOriginalEditions
  R1*9 |
  d'1~\p^\markup "Vcl." |
  d~ |
  d4 r r2

  R1*3 |
  g,4\p^\markup "Bassi" r g r |
  c, r c r |
  d r d r |
  
  b4 r b r |
  R1*4 |
  g'4\p r g r |

				%pagge 10
  \pageCounterOriginalEditions
  c, r c r |
  d r d r |
  b4 r b r |
  R1*6
    a'8\p r d, r a' r c r |
  b4.( \times 2/3 {c16 b a} g4) r |
  
				%page 11
  \pageCounterOriginalEditions
  c,8 r g r c r e r |
  R1 |
  b8 r fis r b r d r |
  R1 |
  a8 r e r a r c r |
  
  R1 |
  r2 r4 g'8 fis |
  e4 r r   e'8 d |
  c4 r r c8 b |
  
  a4 a8 g8 fis4 fis8 e |
  d4.( ees8 d ees d ees) |
  d4.( ees8 d ees d ees) |
  d4 r r2 |
  R1 |
				%page 12 
  \pageCounterOriginalEditions
  R1*5 |
  d4\p r d r |

  d4\p r d r |
  g, r r2 |
  R1*3 |

  R1*4 |

  d'4\p r d r |
  g, r r2 |
  R1 |
  r2 r4 g'8 fis |

				%page 13
  \pageCounterOriginalEditions
  e4 r r e8 d |
  c4 r r c8 b |
  a4 r r2 |
  g'1 |

  c, |
  cis |
  cis |
  <<
    {d,8^\markup "Vcl." fis g  a b c! d fis} \\
    {R1}
  >> |
  
  << 
    {g4 r r2} \\
    R1
  >> |
  R1*2 |
  d4^\markup "Bassi" r r2 |

				%page 14
  \pageCounterOriginalEditions
  d8 d d d d d d d |
  g\f r d r g r b r |
  c4.( \times 2/3 {b16 a g} fis4) r8. d16 |
  fis8 r a r c r a r |
  d4.( \times 2/3 {e16 d  c} b4) r8. d,16 |
  g8 r bes r d r bes r |
  ees2 r4 ees, |

  d2 r4 d'4 |
  ees4. ees,8 d4. d'8 |
  ees ees, d d' ees ees, d d' |
  ees ees ees ees ees ees ees ees |
  d d d d d, d d d |
  g4 r r2 |
  R1 |
  c,8\p c c c cis cis cis cis |

  \pageCounterOriginalEditions
				%page 15
  d d d d d d d d |
  g4 r r2 |
  R1 |
  c,8 c c c cis cis cis cis |
  d d d d dis dis dis dis |
  e e e e ees ees ees ees |
  d! d d d cis cis cis cis |
  c! c c c c c c c |

  b b b b b b b b |
  ais ais ais ais b b b b |
  c c c c c c c c |
  b4 r b r |
  b4 r b r |
  b4 r b r |
  b4 r  r2 |
  R1 |

  R1*7 |
  
				%page 16
  \pageCounterOriginalEditions
  R1 |
  e1\p |
  R1 |
  dis'1( |
  e4) r a, r |

  b r b, r |
  e r r2 |
  R1 |
  e4 r e r |
  e r e e |

				%page 17
  \pageCounterOriginalEditions
  a, r r2 |
  R1 |
  a'4 r a r |
  a r a a |
  d, r r2 |

  R1 |
  d4 r d r |
  d r d d |
  g, r r2 |
  R1 |

				%page 18
  \pageCounterOriginalEditions
  r2 des'( |
  c4) r c2( |
  f4) r r2 |
  R1 |
  
  ees2( f4 g) |
  aes4 r r2 |
  d,2( ees4 f) |
  g2 g, |

  c4 r bes' r |
  aes r e r |
  f r ees r |
  d r f r |
  ees r b! r |
  
				%page 19
  \pageCounterOriginalEditions
  c 4 r r2 |
  R1*4 |

  R1 |
  g'1~ |
  g |
  g,~ |
  g |

				%page 20
  \pageCounterOriginalEditions
  c8\p r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r |
  r8 c' c c c4 r |
  r8 g g g g4 r |
  R1 |

  R1 |
  r8 c c c c4 r |
  r8 g g g g4 r |
  R1 |
  c,8\f r g r c r e r |
  f4.( \times 2/3 {e16 d c } b4) r8. g16

				%page 21
  b8 r d r f r d r |
  g4.( \times 2/3 { a16 g f } d4) r8. g16 |
  c,8 r e r a r c r |
  gis4.( \times 2/3 {a16 gis fis} e4) r8. gis16 |
  a8 r e r c' r a r |
  fis4.( \times 2/3 {g16 fis e} d4) r8. fis16 |
				
  g8 r d r bes' r g r |
  e4.( \times 2/3 {fis!16 e d} c4) r8. e16 |
  f4 r r2 |
  R1*3 |

				%page 22
  \pageCounterOriginalEditions
  R1*6 |
  
  <<
    {g'1\p~-\markup "Vcl." | g1~ | g4 r r2 |}
    \\
    {R1*3 |}
  >>
  R1*2 |
  
  R1*3 |
  c,,4\p^\markup "Bassi" r c r |
  f r f r |

				%page 23
  \pageCounterOriginalEditions
  g4 r g r |
  e r e r |
  R1*4 |
  
  c4 r c r |
  f r f r |
  g r g r |
  e r e r |
  R1*3 |
  
  R1*3 |
  d8\p r g, r d' r f r |
  e4.( \times2/3 {f16 e d} c4) r |
  
				%page 24
  \pageCounterOriginalEditions
  f8 r c r f r a r |
  R1 |
  e8 r b r e r g r |
  R1 |

  d8 r a r d r f r |
  R1 |
  r2 r4 c'8 b |
  a4 r r a8 g |

  f4 r r f'8 e |
  d4 d8 c b4 b8 a |
  g4. (aes8 g aes g aes) |
  g4. (aes8 g aes g aes) |

				%page 25
  \pageCounterOriginalEditions
  g4 r r2 |
  R1 |
  g1~
  g1~

  g4 r r2 |
  R1 |
  c,4\p c c c |
  c c c c |
  g'1 |
  g, |

				%page 26 
  \pageCounterOriginalEditions
  c4 c c c |
  c c c c |
  g'1 |
  g,1 |
  c4 r r2 |
  R1 |
  
  R1*4 |

  g'4\p r g, r |
  c4 r r c8 b |
  a4 r r a'8 g |
  f4 r r f8 e |
				%page 27
  \pageCounterOriginalEditions
  d4 r r2 |
  c1 |
  e( |
  f) |
  fis ( |

  fis4) r r8 aes g fis!|
  g4 r r2 |
  R1*3 |

				%page 28
  \pageCounterOriginalEditions
  R1 |
  g4 r r2 |
  g4 r r2 |
  g8 g g g g g g g |

  g8 g g g g g g g |
  c,8\f r g r c r e r |
  f4.( \times 2/3 {e16 d c} b4) r8. g16 |
  b8 r d r f r d r |
  g4.( \times 2/3 {a16 g f} e4) r8. g16 |
  c,8 r ees r g r c r |

				%page 29
  \pageCounterOriginalEditions
  aes2 r4 aes,4 |
  g2 r4 g'4 |
  aes4. aes,8 g4. g'8 |
  aes8 aes, g g' aes aes, g g' |
  aes aes aes aes aes aes aes aes |
  g4 r f r |
  e! r a! r |
  g2 r\fermata |
  c,8\f c e e f f f f |

  e8 e e e f f f f |
  e e e e f f f f |
  e e e e e e e e |
  f f f f f f f f |
  fis fis fis fis a a a a |
  g g g g g g g g |
  g g g g g g g g |

				%page 30  
  \pageCounterOriginalEditions
  c,4 r r2 |
  R1 |
  f8\p f f f f f f f |
  g g g g g, g g g |
  c4 r r2 |
  
  c'8\f r g r c r e r |
  f4. d16 (b) c4. a16 (fis) |

  g4 r g, r |
  c r r r8. g16\p |
  c8 r e r g4 r8. g,16 |
  c8 r e r g4.( \times 2/3 {f16 e d} |
  c8) r g r c r e r |
  c4 r r2 \bar "|."
}