\version "2.14.0"

\include "global.ly"
\include "violinI-music.ly"
\header {
  instrument = "Violin I"
}

\score {
  \new Staff {
    \global \violinI
  }
}