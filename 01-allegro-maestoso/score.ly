\version "2.14.0"

\include "global.ly"
\include "flute-music.ly"
\include "oboe-music.ly"
\include "bassoon-music.ly"
\include "horn-music.ly"
\include "trumpet-music.ly"
\include "timpani-music.ly"
\include "piano-music.ly"
\include "violinI-music.ly"
\include "violinII-music.ly"
\include "viola-music.ly"
\include "cdb-music.ly"

\layout {
  \context { 
				% add the RemoveEmptyStaffContext that erases rest-only staves
    \Staff \RemoveEmptyStaves 
  }
				% \context {
				%   \Score
				%   % Remove all-rest staves also in the first system
				%   \override VerticalAxisGroup #'remove-first = ##t
				% }
				% \context {
				%   \ChoirStaff 
				%   % If only one non-empty staff in a system exists, still print the backet
				%   \override SystemStartBracket #'collapse-height = #1
				% }
}



 #(set-global-staff-size 12.0)
% #(set-default-paper-size "11x17")

\score {
  <<
    \new StaffGroup = "Woodwinds" 
    <<
      \new Staff {
	\set Staff.instrumentName = #"Flute"
	\set Staff.shortInstrumentName = #"Fl."
	\set Staff.midiInstrument = #"flute"
	\global \fluteMusic
      }
      \new Staff {
	\set Staff.instrumentName = #"Oboe"
	\set Staff.shortInstrumentName = #"Ob."
	\set Staff.midiInstrument = #"oboe"
	\partcombine {\relative c'' {\clef treble \global  \oboeFirst}} {\relative c'' {\clef treble \global \oboeSecond}} 
      }
      \new Staff {
	\set Staff.instrumentName = #"Bassoon"
	\set Staff.shortInstrumentName = #"Ba."
	\set Staff.midiInstrument = #"bassoon"
	\partcombine {\relative c' {\clef bass \global  \bassoonFirst}} {\relative c' {\clef bass \global \bassoonSecond}}
      }
    >>
    \new StaffGroup = "Brass"
    <<
      \new Staff {
	\set Staff.instrumentName = #"Horn"
	\set Staff.shortInstrumentName = #"Ho."
	\set Staff.midiInstrument = #"french horn"
	\partcombine {\relative c'' {\clef treble \global  \hornFirst}} {\relative c' {\clef treble \global \hornSecond}}

      }
      \new Staff {
	\set Staff.instrumentName = #"Trumpet"
	\set Staff.shortInstrumentName = #"Tr."
	\set Staff.midiInstrument = #"trumpet"
	\partcombine {\relative c'' {\clef treble \global  \trumpetFirst}} {\relative c' {\clef treble \global \trumpetSecond}}
      }
    >>
    \new Staff {
      \set Staff.instrumentName = #"Timpani"
      \set Staff.shortInstrumentName = #"Ti."
      \set Staff.midiInstrument = #"timpani"
      \global \Timpani 
    }
    \new PianoStaff  {
      \set PianoStaff.instrumentName = #"Piano"
      \set PianoStaff.shortInstrumentName = #"P."
      \set Staff.midiInstrument = #"acoustic grand"
      <<
	\new Staff = "RH" {
	  \relative c'' {\global \pianoRH} 
	}
	\new Staff = "LH" {
	  \relative c {\global \clef  bass \pianoLH} 
	}
      >>
    }
    \new StaffGroup = "Strings"
    <<
      \new GrandStaff = "Violins"
      <<
	\new Staff {
	  \set Staff.instrumentName = #"Violin I"
	  \set Staff.shortInstrumentName = #"Vi. I"
	  \set Staff.midiInstrument = #"violin"
	  \global \violinI 
	}
	\new Staff {
	  \set Staff.instrumentName = #"Violin II"
	  \set Staff.shortInstrumentName = #"Vi. II"
	  \set Staff.midiInstrument = #"violin"
	  \global \violinII 
	}
      >>
      \new Staff {
	\set Staff.instrumentName = #"Viola"
	\set Staff.shortInstrumentName = #"V.a"
	\set Staff.midiInstrument = #"viola"
	\global \viola
      }
      \new Staff {
	\set Staff.instrumentName = #"Cello Double Bass"
	\set Staff.shortInstrumentName = #"CDB"
	\set Staff.midiInstrument = #"cello"
	\global \cdb
      }
    >>
  >>
  \midi{
    \context {
      \Score
      tempoWholesPerMinute = #(ly:make-moment 120 4)
    }
  }
  \layout{}
}
