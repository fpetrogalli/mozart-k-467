\version "2.14.0"

#(set-default-paper-size "11x17")


pageCounterOriginalEditions={
  \mark\default
}

global = {
  \key c \major
  \time 4/4
  \tempo "Allegro maestoso" 
  \compressFullBarRests
  \set Score.markFormatter = #format-mark-box-numbers
}


\paper {
  % obsolete-between-system-padding = #0.1  system-system-spacing #'padding = #(/ obsolete-between-system-padding staff-space)  score-system-spacing #'padding = #(/ obsolete-between-system-padding staff-space)
  % obsolete-between-system-space = #0.1  system-system-spacing #'basic-distance = #(/ obsolete-between-system-space staff-space)  score-system-spacing #'basic-distance = #(/ obsolete-between-system-space staff-space)
  ragged-last-bottom = ##f
  ragged-bottom = ##f
}

\header { 
    title = \markup  { "Piano Concerto K. 467" }
    composer = "W. A. Mozart"
    tagline = "Edited by Francesco Petrogalli"
}


